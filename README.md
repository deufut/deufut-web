# DeuFut - Website

**Participantes:**

- Daniel Soares Jorge
- Ramon Nobre Peres


**Instruções:**

Primeiramente precisamos configurar o ambiente para desenvolvimento com React JS, baixe e instale a versão estável do Node.js através desse **[link](https://nodejs.org/en/download/)**.

Agora precisamos clonar o projeto através do **[Git](https://git-scm.com/)** por esse **[link](https://gitlab.com/deufut/deufut-web.git)** ou por download direto pelo **[link](https://gitlab.com/deufut/deufut-web/-/archive/master/deufut-web-master.zip)**.


Pelo terminal acesse a pasta do projeto e instale as dependências.

Para instalar via NPM rode o comando:
> _npm install_

Para instalar via YARN rode o comando:
> _yarn_

Pronto! A configuração do projeto está completa e só precisamos rodar o código através dos comandos:


> _npm start_

_OU_

> _yarn start_


**Para acessar o website utilize este [endereço](http://localhost:3000/dashboard) em seu navegador.**
