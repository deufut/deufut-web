import React from 'react';
import { Switch } from 'react-router-dom';
import Login from '~/pages/SignIn';
import Register from '~/pages/Register';
import ForgotPassword from '~/pages/ForgotPassword';
import Dashboard from '~/pages/Dashboard';

import Route from './Route';
import Profile from '~/pages/Profile';
import Requests from '~/pages/Requests';
import Rented from '~/pages/Rented';
import CreateField from '~/pages/CreateField';
import EditField from '~/pages/EditField';
import ChangePassword from '~/pages/ChangePassword';
import Rents from '~/pages/Rents';

export default function Routes() {
  return (
    <>
      <Switch>
        {/* free routes */}
        <Route path="/" exact component={Login} />
        <Route path="/register" pageTitle="Cadastro" component={Register} />
        <Route path="/forgot-password" component={ForgotPassword} />

        {/* private routes */}
        <Route
          path="/dashboard"
          pageTitle="Campos"
          component={Dashboard}
          isPrivate
        />
        <Route
          path="/profile"
          pageTitle="Perfil"
          component={Profile}
          isPrivate
        />
        <Route
          path="/change-password"
          pageTitle="Alterar senha"
          component={ChangePassword}
          isPrivate
        />
        <Route
          path="/requests"
          pageTitle="Solicitações"
          component={Requests}
          isPrivate
        />
        <Route
          path="/rented"
          pageTitle="Aluguéis reservados"
          component={Rented}
          isPrivate
        />
        <Route
          path="/create-field"
          pageTitle="Criar campo"
          component={CreateField}
          isPrivate
        />
        <Route
          path="/rents/:fieldId"
          pageTitle="Aluguéis"
          component={Rents}
          isPrivate
        />
        <Route
          path="/edit-field/:fieldId"
          pageTitle="Editar campo"
          component={EditField}
          isPrivate
        />
      </Switch>
    </>
  );
}
