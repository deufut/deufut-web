import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { store } from '~/store';
import DefaultLayout from '~/pages/_layouts/default';
import UnauthLayout from '~/pages/_layouts/unauth';

export default function RouteWrapper({
  component: Component,
  isPrivate,
  pageTitle,
  ...rest
}) {
  const { signed } = store.getState().auth;

  document.title = `${pageTitle ? `${pageTitle} - ` : ''} DeuFut `;

  if (!signed && isPrivate) {
    return <Redirect to="/" />;
  }

  if (signed && !isPrivate) {
    return <Redirect to="/dashboard" />;
  }

  const Layout = signed ? DefaultLayout : UnauthLayout;

  return (
    <Route
      {...rest}
      render={(props) => (
        <Layout>
          <Component {...props} />
        </Layout>
      )}
    />
  );
}

RouteWrapper.propTypes = {
  component: PropTypes.oneOfType([PropTypes.element, PropTypes.func])
    .isRequired,
  isPrivate: PropTypes.bool,
  pageTitle: PropTypes.string,
};

RouteWrapper.defaultProps = {
  isPrivate: false,
  pageTitle: '',
};
