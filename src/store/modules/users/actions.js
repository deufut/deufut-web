export function addUserInformations(user) {
  return {
    type: '@users/ADD_USER_INFORMATIONS',
    payload: { user },
  };
}

export function removeUserInformations() {
  return {
    type: '@users/REMOVE_USER_INFORMATIONS',
  };
}

export function editUserRequest({ name, phone, password, email }) {
  return {
    type: '@users/EDIT_USER_REQUEST',
    payload: { name, phone, password, email },
  };
}

export function editUserSuccess(data) {
  return {
    type: '@users/EDIT_USER_SUCCESS',
    payload: { data },
  };
}

export function editUserFailure() {
  return {
    type: '@users/EDIT_USER_FAILURE',
  };
}
