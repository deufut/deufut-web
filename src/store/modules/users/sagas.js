import { takeLatest, call, put, all } from 'redux-saga/effects';
import { toast } from 'react-toastify';
import api, { CancelToken } from '../../../services/api';
import { editUserFailure, editUserSuccess } from './actions';
import { deuFutSwal } from '~/components/global/deuFutSwal/deuFutSwal';

let signal = false;

export function* editUserInfo({ payload }) {
  const { name, email, password, phone } = payload;

  try {
    signal = CancelToken.source();
    const response = yield call(
      api.put,
      `/users/update`,
      {
        name,
        phone,
        password,
        email,
      },
      { cancelToken: signal.token }
    );
    yield put(editUserSuccess(response.data));
    deuFutSwal.fire({
      icon: 'success',
      text: 'Alteração feita com sucesso!!',
    });
  } catch (err) {
    toast.error('Erro ao editar perfil, tente novamente mais tarde...');
    yield put(editUserFailure());
  }
}

export default all([takeLatest('@users/EDIT_USER_REQUEST', editUserInfo)]);
