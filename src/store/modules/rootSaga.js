import { all } from 'redux-saga/effects';

import auth from './auth/sagas';
import field from './field/sagas';
import users from './users/sagas';

export default function* rootReducer() {
  return yield all([auth, field, users]);
}
