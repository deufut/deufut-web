export function loadFieldsRequest(start, length) {
  return {
    type: '@field/LOAD_FIELD_REQUEST',
    payload: { start, length },
  };
}

export function loadFieldsSuccess(data) {
  return {
    type: '@field/LOAD_FIELD_SUCCESS',
    payload: { data },
  };
}

export function loadFieldsFailure() {
  return {
    type: '@field/LOAD_FIELD_FAILURE',
  };
}

export function deleteFieldRequest(id) {
  return {
    type: '@field/DELETE_FIELD_REQUEST',
    payload: { id },
  };
}

export function deleteFieldSuccess(id) {
  return {
    type: '@field/DELETE_FIELD_SUCCESS',
    payload: { id },
  };
}

export function deleteFieldFailure() {
  return {
    type: '@field/DELETE_FIELD_FAILURE',
  };
}
