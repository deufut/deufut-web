import { takeLatest, call, put, all, select } from 'redux-saga/effects';
import { toast } from 'react-toastify';
import api, { CancelToken } from '../../../services/api';
import {
  deleteFieldFailure,
  deleteFieldSuccess,
  loadFieldsFailure,
  loadFieldsSuccess,
} from './actions';

let signal = false;

export function* removeField({ payload }) {
  const { id } = payload;

  try {
    yield call(api.delete, `/footballfields/delete/${id}`);

    yield put(deleteFieldSuccess(id));
  } catch (err) {
    toast.error('Erro ao deletar campo, tente novamente mais tarde...');
    yield put(deleteFieldFailure());
  }
}

export function* loadFields() {
  const { start, length } = yield select((state) => state.field);

  try {
    signal = CancelToken.source();
    const response = yield call(
      api.post,
      '/footballfields/get',
      {
        start,
        length,
      },
      { cancelToken: signal.token }
    );

    yield put(loadFieldsSuccess(response.data.data));
  } catch (err) {
    toast.error('Erro ao carregar campos, tente novamente mais tarde...');
    yield put(loadFieldsFailure());
  }
}

export default all([
  takeLatest('@field/DELETE_FIELD_REQUEST', removeField),
  takeLatest('@field/LOAD_FIELD_REQUEST', loadFields),
]);
