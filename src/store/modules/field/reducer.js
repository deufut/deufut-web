import produce from 'immer';

const INITIAL_STATE = {
  fields: [],
  loadingDelete: false,
};

export default function field(state = INITIAL_STATE, action) {
  return produce(state, (draft) => {
    switch (action.type) {
      case '@field/LOAD_FIELD_REQUEST': {
        draft.loading = true;
        break;
      }

      case '@field/LOAD_FIELD_SUCCESS': {
        const { data } = action.payload;

        draft.fields = data;

        draft.loading = false;
        break;
      }

      case '@field/LOAD_FIELD_FAILURE': {
        draft.loading = false;
        break;
      }

      case '@field/DELETE_FIELD_REQUEST': {
        draft.loadingDelete = true;
        break;
      }

      case '@field/DELETE_FIELD_SUCCESS': {
        const { id } = action.payload;

        const filterId = draft.fields.findIndex((f) => f.id === id);

        draft.loadingDelete = false;
        if (filterId >= 0) {
          draft.fields.splice(filterId, 1);
        }

        break;
      }

      case '@field/DELETE_FIELD_FAILURE': {
        draft.loadingDelete = false;
        break;
      }

      default:
        break;
    }
  });
}
