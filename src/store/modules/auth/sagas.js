import { takeLatest, call, put, all } from 'redux-saga/effects';
import { toast } from 'react-toastify';
import api, { CancelToken } from '../../../services/api';
import { signInSuccess, signInFailure } from './actions';
import { addUserInformations, removeUserInformations } from '../users/actions';
import history from '~/services/history';

let signal;
export function* signIn({ payload }) {
  const { email, password } = payload;
  signal = CancelToken.source();

  try {
    const response = yield call(
      api.post,
      '/sessions',
      {
        email,
        password,
      },
      { cancelToken: signal.token }
    );

    const { token, user } = response.data;

    api.defaults.headers.Authorization = `Bearer ${token}`;

    yield put(addUserInformations(user));
    yield put(signInSuccess(token));
    history.push('/dashboard');
  } catch (err) {
    toast.error('Usuário não autenticado, verifique seus dados!');
    yield put(signInFailure());
  }
}

export function* signOut() {
  history.push('/');
  yield put(removeUserInformations());
  delete api.defaults.headers.Authorization;
}

export function setToken({ payload }) {
  if (!payload) return;

  const { token } = payload.auth;

  if (token) {
    api.defaults.headers.Authorization = `Bearer ${token}`;
  }
}

export default all([
  takeLatest('persist/REHYDRATE', setToken),
  takeLatest('@auth/SIGN_IN_REQUEST', signIn),
  takeLatest('@auth/SIGN_OUT', signOut),
]);
