import { combineReducers } from 'redux';

import auth from './auth/reducer';
import users from './users/reducer';
import field from './field/reducer';

export default combineReducers({
  auth,
  users,
  field,
});
