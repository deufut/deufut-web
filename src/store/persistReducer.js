import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';

export default (reducers) => {
  const persistedReducer = persistReducer(
    {
      key: 'DEUFUTv.0.1.0',
      storage,
      whitelist: ['auth', 'users'],
    },
    reducers
  );

  return persistedReducer;
};
