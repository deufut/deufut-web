import React from 'react';
import { Link } from 'react-router-dom';

import { useDispatch, useSelector } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
import { Container, Content, Profile } from './styles';

import logoDeuFut from '../../assets/images/logoDeuFut.svg';
import avatar from '~/assets/images/avatar.svg';
import NavBar from './NavBar';
import { signOut } from '~/store/modules/auth/actions';

function Header() {
  const name = useSelector((state) => state.users.user.name);
  const dispatch = useDispatch();

  return (
    <Container>
      <Content>
        <img src={logoDeuFut} alt="DeuFut" className="logo-deu-fut" />
        <NavBar />

        <aside>
          <Profile>
            <div>
              <strong>{name}</strong>
              <Link to="/profile">Meu perfil</Link>
            </div>
            <img src={avatar} alt="" className="avatar-user" />
            <FontAwesomeIcon
              icon={faSignOutAlt}
              onClick={() => dispatch(signOut())}
            />
          </Profile>
        </aside>
      </Content>
    </Container>
  );
}

export default Header;
