import React from 'react';
import { NavLink } from 'react-router-dom';

import { WrapperHeader, ItemNavbar } from './styles';

function NavBar() {
  return (
    <WrapperHeader>
      <ItemNavbar>
        <NavLink to="/dashboard">Campos</NavLink>
      </ItemNavbar>
      <ItemNavbar>
        <NavLink to="/requests">Solicitações</NavLink>
      </ItemNavbar>
      <ItemNavbar>
        <NavLink to="/rented">Reservados</NavLink>
      </ItemNavbar>
    </WrapperHeader>
  );
}

export default NavBar;
