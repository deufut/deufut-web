import styled from 'styled-components';

export const WrapperHeader = styled.ul`
  padding: 10px 0;
  display: flex;
  align-items: center;
  flex: 1;
`;

export const ItemNavbar = styled.li`
  padding: 10px 0;
  margin-top: -10px;
  margin-bottom: -10px;
  margin-right: 5px;

  a {
    padding: 5px 10px;
    font-size: 1em;
    font-weight: bold;
    white-space: nowrap;
    font-size-adjust: unset;
    text-decoration: none;
    color: #3eb400;

    &:hover {
      border-bottom: 2px solid #3eb400;
    }
  }

  a.active {
    background: #3eb400;
    border-radius: 4px;
    color: #fff;
    border: 2px solid #3eb400;

    &:hover {
      border: 2px solid #3eb400;
    }
    > svg {
      color: #fff;
    }
  }
`;
