import styled from 'styled-components';

export const Container = styled.div`
  background: #fff;
  padding: 0 30px;
`;
export const Content = styled.div`
  height: 64px;
  width: 100%;
  /* max-width: 900px; */
  /* margin: 0 auto; */
  display: flex;
  justify-content: space-between;
  align-items: center;

  img.logo-deu-fut {
    max-height: 55px;
    height: 100%;
    margin-right: 30px;
    padding-right: 30px;
    border-right: 1px solid #eee;
  }

  aside {
    display: flex;
    align-items: center;
  }
`;
export const Profile = styled.div`
  display: flex;
  margin-left: 30px;
  align-items: center;
  padding-left: 30px;
  border-left: 1px solid #eee;

  div {
    text-align: right;
    margin-right: 10px;

    strong {
      display: block;
      color: #333;
    }

    a {
      display: block;
      margin-top: 2px;
      font-size: 12px;
      color: #999;
    }
  }
  svg {
    font-size: 20px;
    margin-left: 12px;
    cursor: pointer;
  }
  img.avatar-user {
    height: 32px;
    border-radius: 50%;
    fill: #3a3a;
  }
`;
