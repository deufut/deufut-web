import React, { useCallback, useState } from 'react';
import { faSpinner, faTimes } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import PropTypes from 'prop-types';

import { Input } from '@rocketseat/unform';
import { format } from 'date-fns';
import {
  Wrapper,
  Header,
  TextInfo,
  DatePicker,
  FormInput,
  ButtonSubmit,
} from './styles';
import { InputChange } from '~/utils/inputHandler';

export default function NewRent({ close, handleCreate, loading }) {
  const [startDate, setStartDate] = useState(new Date());
  const [values, setValues] = useState([]);

  const handleSubmit = useCallback(
    (params) => {
      const data = {
        ...params,
        date: format(startDate, 'yyyy-MM-dd'),
      };

      handleCreate(data);
    },
    [handleCreate, startDate]
  );

  const handleOnchange = useCallback(
    (event) => {
      setValues(InputChange(event, values));
    },
    [values]
  );

  return (
    <Wrapper>
      <Header>
        <h1> Novo aluguel </h1>
        <button type="button" onClick={close}>
          <FontAwesomeIcon icon={faTimes} />
        </button>
      </Header>
      <TextInfo>
        Para criar um novo aluguel, preenche os campos abaixo.
      </TextInfo>
      <FormInput onSubmit={handleSubmit}>
        <div id="type-date-wrapper">
          <div>
            <h4>Data</h4>
            <DatePicker
              selected={startDate}
              onChange={(date) => setStartDate(date)}
              minDate={new Date()}
              dateFormat="dd/MM/yyyy"
              showPopperArrow={false}
            />
          </div>
        </div>
        <div id="type-date-wrapper">
          <div>
            <h4>Inicio</h4>
            <Input
              name="time_start"
              placeholder="Horário inicial do aluguel"
              type="text"
              onChange={handleOnchange}
            />
          </div>
        </div>
        <div id="type-date-wrapper">
          <div>
            <h4>Término</h4>
            <Input
              name="time_end"
              placeholder="Horário final do aluguel"
              type="text"
              onChange={handleOnchange}
            />
          </div>
        </div>
        <div id="type-date-wrapper">
          <div>
            <h4>Valor</h4>
            <Input
              name="value"
              placeholder="Valor do aluguel"
              type="text"
              onChange={handleOnchange}
            />
          </div>
        </div>
        <div id="buttons-wrapper">
          <ButtonSubmit type="submit" disabled={loading}>
            {!loading ? 'CRIAR' : <FontAwesomeIcon icon={faSpinner} spin />}
          </ButtonSubmit>
        </div>
      </FormInput>
    </Wrapper>
  );
}

NewRent.defaultProps = {
  loading: false,
};

NewRent.propTypes = {
  close: PropTypes.func.isRequired,
  handleCreate: PropTypes.func.isRequired,
  loading: PropTypes.bool,
};
