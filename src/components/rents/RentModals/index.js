import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import EditRent from './EditRent';
import NewRent from './NewRent';

function RentModals({
  close,
  rentId,
  handleCreate,
  loadingCreate,
  loadingEdit,
  handleEdit,
}) {
  const [editSection, setEditSection] = useState(false);

  useEffect(() => {
    if (rentId) {
      setEditSection(true);
    }
  }, [rentId]);

  return (
    <>
      {!editSection && (
        <NewRent
          close={close}
          handleCreate={handleCreate}
          loading={loadingCreate}
        />
      )}

      {editSection && (
        <EditRent
          close={close}
          rentId={rentId}
          handleEdit={handleEdit}
          loading={loadingEdit}
        />
      )}
    </>
  );
}

RentModals.defaultProps = {
  loadingCreate: false,
  loadingEdit: false,
};

RentModals.propTypes = {
  close: PropTypes.func.isRequired,
  rentId: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  handleCreate: PropTypes.func.isRequired,
  handleEdit: PropTypes.func.isRequired,
  loadingCreate: PropTypes.bool,
  loadingEdit: PropTypes.bool,
};

export default RentModals;
