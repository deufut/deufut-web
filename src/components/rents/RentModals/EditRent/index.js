import React, { useCallback, useEffect, useState } from 'react';
import { faSpinner, faTimes } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import PropTypes from 'prop-types';

import { Input } from '@rocketseat/unform';
import { format } from 'date-fns';
import {
  Wrapper,
  Header,
  TextInfo,
  DatePicker,
  FormInput,
  ButtonSubmit,
} from './styles';

import { getRentById } from '~/services/endpoints/rents';

export default function EditRent({ close, rentId, handleEdit, loading }) {
  const [startDate, setStartDate] = useState(new Date());
  const [valueRent, setValueRent] = useState();
  const [timeStart, setTimeStart] = useState();
  const [timeEnd, setTimeEnd] = useState();
  const [loadingInfo, setLoadingInfo] = useState(false);

  const handleSubmit = useCallback(() => {
    const data = {
      time_start: timeStart,
      time_end: timeEnd,
      value: valueRent,
      date: format(startDate, 'yyyy-MM-dd'),
    };
    handleEdit(data);
  }, [handleEdit, startDate, timeStart, timeEnd, valueRent]);

  const loadInfoRent = useCallback(async () => {
    const { apiCall } = getRentById();

    setLoadingInfo(true);
    try {
      const response = await apiCall({ rentId });
      const { data } = response;
      const dateStart = new Date(data.date_start);
      const dateEnd = new Date(data.date_end);

      setValueRent(data.value);
      setTimeStart(
        dateStart.getHours() < 10
          ? `0${dateStart.getHours()}:${String(dateStart.getMinutes()).padStart(
              2,
              '0'
            )}`
          : `${dateStart.getHours()}:${String(dateStart.getMinutes()).padStart(
              2,
              '0'
            )}`
      );
      setTimeEnd(
        dateEnd.getHours() < 10
          ? `0${dateEnd.getHours()}:${String(dateEnd.getMinutes()).padStart(
              2,
              '0'
            )}`
          : `${dateEnd.getHours()}:${String(dateEnd.getMinutes()).padStart(
              2,
              '0'
            )}`
      );
      setLoadingInfo(false);
    } catch (err) {
      setLoadingInfo(false);
    }
  }, [rentId]);

  useEffect(() => {
    loadInfoRent();
  }, [loadInfoRent]);

  return (
    <Wrapper>
      <Header>
        <h1> Editar aluguel </h1>
        <button type="button" onClick={close}>
          <FontAwesomeIcon icon={faTimes} />
        </button>
      </Header>
      <TextInfo>Para editar o aluguel, preenche os campos abaixo.</TextInfo>
      <FormInput onSubmit={handleSubmit}>
        <div id="type-date-wrapper">
          <div>
            <h4>Data</h4>
            <DatePicker
              selected={startDate}
              onChange={(date) => setStartDate(date)}
              minDate={new Date()}
              dateFormat="dd/MM/yyyy"
              showPopperArrow={false}
              disabled={loadingInfo}
            />
          </div>
        </div>
        <div id="type-date-wrapper">
          <div>
            <h4>Inicio</h4>
            <Input
              name="time_start"
              value={timeStart}
              placeholder="Horário inicial do aluguel"
              type="text"
              disabled={loadingInfo}
              onChange={(e) => setTimeStart(e.target.value)}
            />
          </div>
        </div>
        <div id="type-date-wrapper">
          <div>
            <h4>Término</h4>
            <Input
              name="time_end"
              value={timeEnd}
              placeholder="Horário final do aluguel"
              type="text"
              disabled={loadingInfo}
              onChange={(e) => setTimeEnd(e.target.value)}
            />
          </div>
        </div>
        <div id="type-date-wrapper">
          <div>
            <h4>Valor</h4>
            <Input
              name="value"
              value={valueRent}
              placeholder="Valor do aluguel"
              type="text"
              disabled={loadingInfo}
              onChange={(e) => setValueRent(e.target.value)}
            />
          </div>
        </div>
        <div id="buttons-wrapper">
          <ButtonSubmit type="submit" disabled={loadingInfo || loading}>
            {!loading ? 'EDITAR' : <FontAwesomeIcon icon={faSpinner} spin />}
          </ButtonSubmit>
        </div>
      </FormInput>
    </Wrapper>
  );
}

EditRent.defaultProps = {
  loading: false,
};

EditRent.propTypes = {
  close: PropTypes.func.isRequired,
  rentId: PropTypes.number.isRequired,
  handleEdit: PropTypes.func.isRequired,
  loading: PropTypes.func,
};
