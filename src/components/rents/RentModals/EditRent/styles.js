import styled, { keyframes } from 'styled-components';
import DateSelector from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { Form } from '@rocketseat/unform';

const forward = keyframes`
0% {
  margin-left: 0;
} 50% {
  margin-left: 15px;
} 100% {
  margin-left: 0;
}
`;

export const Wrapper = styled.div`
  padding: 54px 68px;
  overflow-y: auto;
`;

export const Header = styled.div`
  display: flex;
  justify-content: space-between;

  > h1 {
    margin-bottom: 10px;
    color: #333;
    font-size: 1.8125rem;
    font-weight: bold;
  }

  > button {
    display: flex;
    padding: 0 10px;
    background: transparent;
    > svg {
      font-size: 1.5rem;
      color: #006400;
      cursor: pointer;
    }
  }
`;

export const TextInfo = styled.p`
  font-size: 0.9rem;
  padding-bottom: 20px;
  margin-bottom: 20px;
  border-bottom: 0.5px solid #d9d9d9;
`;

export const DatePicker = styled(DateSelector)`
  height: 50px;
  width: 100%;
  border: 0;
  border-radius: 24px;
  background: #f0f2f7;
  padding-left: 15px;
  color: #a6b1c3;
`;

export const FormInput = styled(Form)`
  width: 500px;

  h4 {
    font-size: 1.125rem;
    font-weight: 500;
    opacity: 0.9;
    color: #1f2d50;
    margin-bottom: 21px;
  }

  #type-input {
    padding-right: 10px;
  }

  div#type-date-wrapper {
    display: flex;
    margin-bottom: 34px;

    > div {
      width: 100%;
      > input {
        height: 50px;
        width: 100%;
        border: 0;
        border-radius: 24px;
        background: #f0f2f7;
        padding-left: 15px;
        color: #a6b1c3;

        &::placeholder {
          color: #a6b1c3;
        }
      }
    }

    > div:last-child {
      .react-datepicker-wrapper {
        display: flex;
      }
    }

    span {
      display: block;
      margin-top: 10px;
      color: #f32525;
      animation: ${forward} 500ms ease-in-out;
    }
  }
  > div#buttons-wrapper {
    display: flex;
    justify-content: flex-end;
  }
`;

export const ButtonSubmit = styled.button`
  height: 50px;
  width: 100%;
  min-width: fit-content;
  border-radius: 50px;
  padding: 0 10px;

  color: #fff;
  background: #006400;
  transition: background 0.7s;
  font-size: 14px;
  font-weight: bold;
  text-transform: uppercase;
  white-space: nowrap;

  display: flex;
  justify-content: center;
  align-items: center;

  &:focus {
    box-shadow: 0 3px 1px -2px rgba(0, 0, 0, 0.2),
      0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12);
  }

  :disabled {
    opacity: 0.7;
    cursor: ${(props) => (props.loading ? 'progress' : 'now-allowed')};
  }

  svg {
    font-size: 25px;
  }
`;
