import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  background: none;
  width: 100%;
  align-items: center;
  justify-content: center;
  font-size: 6px;
  padding: 3rem;

  .spinner {
    width: 5rem;
    height: 5rem;
    color: #006400;
  }
`;
