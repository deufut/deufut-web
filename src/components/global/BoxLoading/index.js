import PropTypes from 'prop-types';
import React from 'react';

import DotLoader from 'react-spinners/DotLoader';
import { Wrapper } from './styles';

export default function BoxLoading({ loading }) {
  return (
    <Wrapper>
      <DotLoader size={80} color="#006400" loading={loading} />
    </Wrapper>
  );
}

BoxLoading.propTypes = {
  loading: PropTypes.bool.isRequired,
};
