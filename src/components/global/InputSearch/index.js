import React, { useCallback, useRef, useState } from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { InputWrapper, Wrapper } from './styles';

function InputSearch({ icon, ...rest }) {
  const inputRef = useRef(null);
  const [isFocused, setIsFocused] = useState(false);
  const [isFilled, setIsFilled] = useState(false);

  const handleInputFocus = useCallback(() => {
    setIsFocused(true);
  }, []);

  const handleInputBlur = useCallback(() => {
    setIsFocused(false);
    setIsFilled(!!inputRef.current?.value);
  }, []);

  return (
    <Wrapper>
      <InputWrapper isFilled={isFilled} isFocused={isFocused}>
        {icon && <FontAwesomeIcon icon={icon} />}
        <input
          onFocus={handleInputFocus}
          onBlur={handleInputBlur}
          ref={inputRef}
          autoComplete="off"
          {...rest}
        />
      </InputWrapper>
    </Wrapper>
  );
}

export default InputSearch;
