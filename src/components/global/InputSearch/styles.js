import styled, { css } from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  span {
    color: red;
    margin-top: 10px;
  }
`;

export const InputWrapper = styled.div`
  display: flex;
  background: #fff;
  border-radius: 8px;
  border: 1px solid #dcdce6;
  width: 330px;
  align-items: center;
  color: #666369;
  ${(props) =>
    props.error &&
    css`
      color: red;
      border-color: red;
    `}
  ${(props) =>
    props.isFocused &&
    css`
      color: #;
      border-color: #006400;
    `}
  ${(props) =>
    props.isFilled &&
    css`
      color: #006400;
    `}
  input {
    flex: 1;
    background: transparent;
    border: 0;
    width: 85%;
    color: #666360;
    padding: 16px 0;
    &::placeholder {
      color: #666360;
    }
  }
  svg {
    width: 15% !important;
    color: #006400;
  }
`;
