import styled, { keyframes } from 'styled-components';
import Modal from 'react-modal';

const animationOpen = keyframes`
  from {
    transform: scale(0);
    opacity:0.5;
  }
  to {
    transform: scale(1);
    opacity: 1;
  }
`;

export const ModalStyles = styled(Modal)`
  max-height: 90vh;
  max-width: 100vw;

  border-radius: 10px;
  background: #ffff;
  overflow: hidden;

  animation: ${animationOpen} 300ms ease;

  > div {
    max-height: 90vh;
  }
`;
