import React, { Children, cloneElement, isValidElement, useState } from 'react';
import PropTypes from 'prop-types';
import './styles.css';

import { ModalStyles } from './styles';

function ModalCenter({ children, isOpen, close, ...rest }) {
  const [overlayRef, setOverlayRef] = useState();

  const childrenWithProps = Children.map(children, (child) => {
    if (isValidElement(child)) {
      return cloneElement(child, { isOpen, close, overlayRef, rest });
    }
    return child;
  });

  return (
    <ModalStyles
      contentRef={(ref) => setOverlayRef(ref)}
      isOpen={isOpen}
      onRequestClose={close}
      appElement={document.getElementById('root')}
      shouldCloseOnEsc
      overlayClassName="modal-center-overlay"
    >
      {childrenWithProps}
    </ModalStyles>
  );
}

ModalCenter.propTypes = {
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.any]).isRequired,
  close: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
};

export default ModalCenter;
