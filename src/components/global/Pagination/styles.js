import styled, { css } from 'styled-components';

export const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 6px;
`;

export const ButtonArrow = styled.button`
  width: 31px;
  height: 31px;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 13px;
  cursor: pointer;
  font-weight: bold;
  border-radius: 50px;
  margin-left: 12px;
  margin-right: 12px;
  svg {
    font-size: 14px;
    color: #006400;
  }

  &:disabled {
    ${(props) =>
      props.loading && !props.reach
        ? css`
            cursor: progress;
          `
        : undefined}

    ${(props) =>
      props.reach
        ? css`
            opacity: 0.4;
            cursor: not-allowed;
          `
        : undefined}
  }
`;

export const ButtonPage = styled.button`
  width: 31px;
  height: 31px;
  margin-left: 6px;
  margin-right: 5px;
  cursor: pointer;
  background: ${(props) => (props.isSelected ? '#006400' : 'none')};
  color: ${(props) => (props.isSelected ? '#fff' : '#006400')};
  border: ${(props) => (props.isSelected ? '0' : '1px solid #006400')};

  &[disabled] {
    cursor: ${(props) => props.loading && 'progress'};
  }
`;
