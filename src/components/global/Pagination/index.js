import React, { useEffect, useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import PropTypes from 'prop-types';
import {
  faChevronLeft,
  faChevronRight,
} from '@fortawesome/free-solid-svg-icons';
import { Container, ButtonPage, ButtonArrow } from './styles';

export default function Pagination({ totalPages, changePage, page, loading }) {
  const [pagesJSX, setPagesJSX] = useState([]);

  useEffect(() => {
    function handleRenderPages() {
      const data = [];

      for (let i = 1; i <= totalPages; i += 1) {
        data.push(i);
      }

      setPagesJSX(data);
    }
    handleRenderPages();
  }, [totalPages]);

  const handleChangePage = (newPage) => {
    if (newPage <= 0 || newPage > totalPages || newPage === page) return;
    changePage(newPage);
  };

  return (
    <Container>
      <ButtonArrow
        type="button"
        disabled={page === 1 || loading ? 1 : 0}
        loading={loading ? 1 : 0}
        reach={page === 1 ? 1 : 0}
        onClick={() => handleChangePage(page - 1)}
      >
        <FontAwesomeIcon icon={faChevronLeft} />
      </ButtonArrow>
      {pagesJSX.map((pageNumber) => (
        <ButtonPage
          key={pageNumber}
          isSelected={page === pageNumber ? 1 : 0}
          type="button"
          disabled={loading}
          loading={loading ? 1 : 0}
          onClick={() => handleChangePage(pageNumber)}
        >
          {pageNumber}
        </ButtonPage>
      ))}
      <ButtonArrow
        type="button"
        disabled={page === totalPages || loading ? 1 : 0}
        loading={loading ? 1 : 0}
        reach={page === totalPages ? 1 : 0}
        onClick={() => handleChangePage(page + 1)}
      >
        <FontAwesomeIcon icon={faChevronRight} />
      </ButtonArrow>
    </Container>
  );
}

Pagination.propTypes = {
  totalPages: PropTypes.number.isRequired,
  changePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  loading: PropTypes.bool,
};
Pagination.defaultProps = {
  loading: false,
};
