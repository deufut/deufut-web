export const InputChange = (event, input) => {
  return { ...input, [event.target.name]: event.target.value };
};
