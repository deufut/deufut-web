import React, { useCallback, useEffect, useState } from 'react';

import { Input, Form } from '@rocketseat/unform';
import * as Yup from 'yup';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import { useDispatch, useSelector } from 'react-redux';

import { Link } from 'react-router-dom';
import {
  Container,
  Wrapper,
  HeaderBox,
  FieldRegister,
  InfoRow,
  FieldWrapper,
  BoxButton,
} from './styles';
import { editUserRequest } from '~/store/modules/users/actions';

const schema = Yup.object().shape({
  name: Yup.string().required('O nome do campo é obrigatório'),
  phone: Yup.string()
    .min(11, 'Número de celular inválido!! (DDD + 9 + xxxxxxxx')
    .required('O número de celular é obrigatório'),
  email: Yup.string()
    .email('Insira um e-mail válido')
    .required('O e-mail é obrigatório'),
});

const signal = false;

function Profile() {
  const user = useSelector((state) => state.users.user);
  const loading = useSelector((state) => state.users.loading);
  const dispatch = useDispatch();

  const [value, setValue] = useState({
    name: user.name,
    email: user.email,
    phone: user.phone,
  });

  function handleEditProfile() {
    dispatch(
      editUserRequest({
        name: value.name,
        phone: value.phone,
        email: value.email,
      })
    );
  }

  useEffect(() => {
    return () => {
      if (signal) {
        signal.cancel('Requisição Cancelada');
      }
    };
  }, []);

  const handleChange = useCallback(
    (e) => setValue({ ...value, [e.target.name]: e.target.value }),
    [value]
  );

  return (
    <Container>
      <Wrapper>
        <HeaderBox>
          <span>Editar perfil</span>
        </HeaderBox>
        <FieldRegister>
          <Form schema={schema} onSubmit={handleEditProfile}>
            <InfoRow>
              <FieldWrapper wd={100}>
                <Input
                  placeholder="Nome"
                  type="text"
                  name="name"
                  value={value.name ? value.name : ''}
                  onChange={handleChange}
                />
              </FieldWrapper>

              <FieldWrapper wd={100}>
                <Input
                  placeholder="Celular"
                  type="text"
                  name="phone"
                  value={value.phone ? value.phone : ''}
                  onChange={handleChange}
                />
              </FieldWrapper>

              <FieldWrapper wd={100}>
                <Input
                  placeholder="E-mail"
                  type="text"
                  name="email"
                  value={value.email ? value.email : ''}
                  onChange={handleChange}
                />
              </FieldWrapper>

              <Link to="/change-password">Alterar sua senha</Link>

              {/* <FieldWrapper wd={100}>
                <Input
                  placeholder="Senha"
                  type="password"
                  name="password"
                  onChange={handleChange}
                />
              </FieldWrapper>

              <FieldWrapper wd={100}>
                <Input
                  placeholder="Confirme sua senha"
                  type="password"
                  name="confirmPassword"
                  onChange={handleChange}
                />
              </FieldWrapper> */}
            </InfoRow>
            <BoxButton>
              <button type="submit">
                {loading ? <FontAwesomeIcon icon={faSpinner} spin /> : 'Editar'}
              </button>
            </BoxButton>
          </Form>
        </FieldRegister>
      </Wrapper>
    </Container>
  );
}

export default Profile;
