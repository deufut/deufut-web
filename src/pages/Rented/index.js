import React, { useCallback, useEffect, useState } from 'react';
import { faSadTear, faTimes } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { toast } from 'react-toastify';
import BoxLoading from '~/components/global/BoxLoading';
import { getRented, kickTeam } from '~/services/endpoints/rents';

import {
  Container,
  Header,
  TitleHeader,
  Wrapper,
  BoxItem,
  BoxField,
  Category,
  ReponseCategory,
  FieldsEmpty,
} from './styles';
import { deuFutSwal } from '~/components/global/deuFutSwal/deuFutSwal';
import exclamationSvg from '~/assets/icons/exclamation-mark.svg';

function Requests() {
  const [rents, setRents] = useState([]);
  const [loading, setLoading] = useState(true);

  const loadRenterReserved = useCallback(async () => {
    const { apiCall } = getRented();

    setLoading(true);
    try {
      const response = await apiCall();

      setRents(response.data.data.rents);
      setLoading(false);
    } catch (err) {
      setLoading(false);
      toast.error(
        'Falha ao carregar aluguéis pendente, tente novamente mais tarde...'
      );
    }
  }, []);

  useEffect(() => {
    loadRenterReserved();
  }, [loadRenterReserved]);

  const handleKickTeam = useCallback(
    async (id) => {
      deuFutSwal
        .fire({
          title:
            'Tem certeza de que deseja desvincular o aluguel com este time?',
          text: 'Você não poderá reverter isso!',
          imageUrl: exclamationSvg,
          showCloseButton: true,
          showCancelButton: true,
          reverseButtons: true,
          confirmButtonText: 'Sim, Deletar!',
          cancelButtonText: 'Cancelar'.toUpperCase(),
          focusCancel: true,
        })
        .then(async (action) => {
          if (action.value) {
            const { apiCall } = kickTeam();

            try {
              await apiCall({ renter_id: id });

              loadRenterReserved();
              toast.success('Time desvinculado com sucesso!');
            } catch (err) {
              toast.error(
                'Erro ao desvincular o time do aluguel, tente novamente mais tarde...'
              );
            }
          }
        });
    },
    [] //eslint-disable-line
  );

  return (
    <Container>
      <Header>
        <TitleHeader>Aluguéis reservados</TitleHeader>
      </Header>
      {loading && (
        <div className="loading">
          <BoxLoading loading={loading} />
        </div>
      )}
      {!loading && rents.length > 0 && (
        <Wrapper>
          {rents.map((rent) => {
            const dateStart = new Date(rent.date_start);
            const dateEnd = new Date(rent.date_end);

            return (
              <BoxItem key={rent.id}>
                <BoxField>
                  <Category>Nome do campo: </Category>
                  <ReponseCategory>{rent.footballfield.name}</ReponseCategory>
                </BoxField>
                <BoxField>
                  <Category>Time responsável: </Category>
                  <ReponseCategory>{rent.renter.team.name}</ReponseCategory>
                </BoxField>
                <BoxField>
                  <Category>Data: </Category>
                  <ReponseCategory>
                    {dateStart.getUTCDate() <= 9
                      ? `0${dateStart.getUTCDate()}`
                      : dateStart.getUTCDate()}
                    /
                    {dateStart.getUTCMonth() + 1 <= 9
                      ? `0${dateStart.getUTCMonth() + 1}`
                      : dateStart.getUTCMonth() + 1}
                    /{dateStart.getUTCFullYear()}
                  </ReponseCategory>
                </BoxField>
                <BoxField className="time">
                  <Category>Inicio: </Category>
                  <ReponseCategory>
                    {dateStart.getHours() < 10
                      ? `0${dateStart.getHours()}`
                      : dateStart.getHours()}
                    :{String(dateStart.getMinutes()).padStart(2, '0')}
                  </ReponseCategory>
                </BoxField>
                <BoxField className="time">
                  <Category>Término: </Category>
                  <ReponseCategory>
                    {dateEnd.getHours() < 10
                      ? `0${dateEnd.getHours()}`
                      : dateEnd.getHours()}
                    :{String(dateEnd.getMinutes()).padStart(2, '0')}
                  </ReponseCategory>
                </BoxField>
                <BoxField className="buttons">
                  <FontAwesomeIcon
                    id="refuse"
                    icon={faTimes}
                    color="#F32525"
                    onClick={() => handleKickTeam(rent.renter.id)}
                  />
                </BoxField>
              </BoxItem>
            );
          })}
        </Wrapper>
      )}
      {!loading && rents.length === 0 && (
        <FieldsEmpty>
          <span>Nenhum aluguel reservado...</span>
          <FontAwesomeIcon icon={faSadTear} />
        </FieldsEmpty>
      )}
    </Container>
  );
}

export default Requests;
