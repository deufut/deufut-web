import { Input } from '@rocketseat/unform';
import styled, { keyframes } from 'styled-components';

const forward = keyframes`
0% {
  margin-left: 0;
} 50% {
  margin-left: 15px;
} 100% {
  margin-left: 0;
}
`;

export const Container = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const Wrapper = styled.div`
  width: 100%;
  max-width: 1200px;
  height: 100%;
  background: #fff;
  border-radius: 25px;
  padding: 10px;
`;

export const HeaderBox = styled.header`
  padding: 20px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-bottom: 20px;
  border-bottom: 1px solid #006400;
  position: relative;

  svg {
    position: absolute;
    left: 26px;
    top: 24px;
    color: #006400;
    cursor: pointer;
  }

  span {
    color: #333;
    font-size: 20px;
    font-weight: bold;
  }
`;

export const FieldRegister = styled.div`
  margin-top: 20px;
  display: flex;
  flex-direction: column;
  padding: 20px;

  span {
    margin-bottom: 10px;
    margin-left: 6px;
    font-size: 13px;
    color: #f64c75;
    align-self: flex-start;
    font-weight: bold;
    animation: ${forward} 500ms ease-in-out;
  }

  input {
    width: 100%;
    resize: vertical;
    border: 1px solid #dcdce6;
    border-radius: 8px;
    padding: 16px 24px;
    margin-bottom: 10px;
    height: 57px;
  }
`;

export const InfoRow = styled.div`
  display: flex;
  flex-wrap: wrap;

  a {
    margin-left: 8px;
    margin-top: 10px;
    color: #3eb400;
    font-size: 15px;
  }
`;

export const FieldWrapper = styled.div`
  width: ${(props) => (props.wd ? `${props.wd}%` : '100%')};
  display: flex;
  flex-direction: column;
  padding: 0 8px;
`;

export const TextDescription = styled(Input)`
  height: 170px;
  width: 100%;
  resize: none;
  border: 1px solid #dcdce6;
  border-radius: 8px;
  padding: 16px 24px;
  margin-bottom: 10px;
`;

export const BoxButton = styled.div`
  padding: 0 8px;
  margin-top: 20px;

  button {
    width: 100%;
    height: 45px;
    background: #3eb400;
    border: 0;
    border-radius: 8px;
    color: #fff;
    cursor: pointer;
    font-weight: 700;
    display: inline-block;
    text-align: center;
    text-decoration: none;
    font-size: 14px;
    line-height: 30px;
    transition: filter 0.2s;
  }
`;
