import React, { useCallback, useEffect, useState } from 'react';

import { Input, Form } from '@rocketseat/unform';
import * as Yup from 'yup';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft, faSpinner } from '@fortawesome/free-solid-svg-icons';
import { useDispatch, useSelector } from 'react-redux';

import {
  Container,
  Wrapper,
  HeaderBox,
  FieldRegister,
  InfoRow,
  FieldWrapper,
  BoxButton,
} from './styles';
import { editUserRequest } from '~/store/modules/users/actions';
import history from '~/services/history';

const schema = Yup.object().shape({
  password: Yup.string().required('A senha é obrigatória'),
  confirmPassword: Yup.string()
    .required('Confirmação de senha é obrigatória')
    .oneOf([Yup.ref('password')], 'Senhas não correspondem'),
});

const signal = false;

function ChangePassword() {
  const loading = useSelector((state) => state.users.loading);
  const dispatch = useDispatch();

  const [value, setValue] = useState({});

  function handleEditProfile() {
    dispatch(
      editUserRequest({
        password: value.password,
      })
    );
  }

  useEffect(() => {
    return () => {
      if (signal) {
        signal.cancel('Requisição Cancelada');
      }
    };
  }, []);

  const handleChange = useCallback(
    (e) => setValue({ ...value, [e.target.name]: e.target.value }),
    [value]
  );

  return (
    <Container>
      <Wrapper>
        <HeaderBox>
          <FontAwesomeIcon
            onClick={() => {
              history.goBack();
            }}
            icon={faArrowLeft}
          />
          <span>Alterar senha</span>
        </HeaderBox>
        <FieldRegister>
          <Form schema={schema} onSubmit={handleEditProfile}>
            <InfoRow>
              <FieldWrapper wd={100}>
                <Input
                  placeholder="Senha"
                  type="password"
                  name="password"
                  onChange={handleChange}
                />
              </FieldWrapper>

              <FieldWrapper wd={100}>
                <Input
                  placeholder="Confirme sua senha"
                  type="password"
                  name="confirmPassword"
                  onChange={handleChange}
                />
              </FieldWrapper>

              {/* <FieldWrapper wd={100}>
                <Input
                  placeholder="Senha"
                  type="password"
                  name="password"
                  onChange={handleChange}
                />
              </FieldWrapper>

              <FieldWrapper wd={100}>
                <Input
                  placeholder="Confirme sua senha"
                  type="password"
                  name="confirmPassword"
                  onChange={handleChange}
                />
              </FieldWrapper> */}
            </InfoRow>
            <BoxButton>
              <button type="submit">
                {loading ? (
                  <FontAwesomeIcon icon={faSpinner} spin />
                ) : (
                  'Alterar'
                )}
              </button>
            </BoxButton>
          </Form>
        </FieldRegister>
      </Wrapper>
    </Container>
  );
}

export default ChangePassword;
