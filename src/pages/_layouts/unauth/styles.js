import styled from 'styled-components';

export const Wrapper = styled.div`
  background: #f0f0f5;

  min-height: 100%;
  max-height: 100%;

  display: flex;
  flex-direction: column;
`;

export const ContentSection = styled.div`
  display: flex;
  flex: 1;
`;
