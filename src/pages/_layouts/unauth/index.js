import PropTypes from 'prop-types';
import React from 'react';

import { Wrapper } from './styles';

function UnauthLayout({ children }) {
  return <Wrapper>{children}</Wrapper>;
}

UnauthLayout.propTypes = {
  children: PropTypes.element.isRequired,
};

export default UnauthLayout;
