import styled from 'styled-components';

export const Container = styled.div`
  padding: 20px 20px 20px 20px;
  width: 100%;

  div.loading {
    height: 80vh;
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;

export const Header = styled.header`
  display: flex;
  align-items: center;
  justify-content: start;
  min-height: 45px;
`;

export const TitleHeader = styled.h1`
  margin-bottom: 0;
  color: #1f2d50;
  font-size: 1.5625rem;
  font-weight: bold;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
`;

export const Wrapper = styled.div``;

export const BoxItem = styled.div`
  background-color: #fff;
  display: flex;
  flex-direction: row;
  margin: 20px 0;
  padding: 20px 10px;
  width: 100%;

  align-items: center;
  box-shadow: 2px 3px 5px #00000029;

  div.time {
    width: 18%;
  }
  div.buttons {
    width: 4%;
    justify-content: center;

    svg {
      cursor: pointer;
    }

    > svg#refuse {
      cursor: pointer;
      margin-right: 10px;
    }
  }
`;

export const BoxField = styled.div`
  display: flex;
  width: 20%;
  align-items: center;
`;

export const Category = styled.span`
  font-size: 18px;
  color: #a6b1c3;
  font-weight: bold;
`;

export const ReponseCategory = styled.span`
  margin-left: 10px;
  font-weight: 500;
`;

export const FieldsEmpty = styled.div`
  height: 80vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  span {
    font-size: 30px;
    font-weight: bold;
    color: #a6b1c3;
  }

  svg {
    font-size: 60px;
    color: #a6b1c3;
    margin-top: 20px;
  }
`;
