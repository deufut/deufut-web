import { faCheck, faSadTear, faTimes } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useCallback, useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import BoxLoading from '~/components/global/BoxLoading';
import { deuFutSwal } from '~/components/global/deuFutSwal/deuFutSwal';
import exclamationSvg from '~/assets/icons/exclamation-mark.svg';
import { getRentRequest, answerRent } from '~/services/endpoints/rents';

import {
  Container,
  Header,
  TitleHeader,
  Wrapper,
  BoxItem,
  BoxField,
  Category,
  ReponseCategory,
  FieldsEmpty,
} from './styles';

function Requests() {
  const [rents, setRents] = useState([]);
  const [loading, setLoading] = useState(true);

  const loadRentsPending = useCallback(async () => {
    const { apiCall } = getRentRequest();

    setLoading(true);
    try {
      const response = await apiCall();
      setRents(response.data.data);
      setLoading(false);
    } catch (err) {
      setLoading(false);
      toast.error(
        'Falha ao carregar aluguéis pendente, tente novamente mais tarde...'
      );
    }
  }, []);

  useEffect(() => {
    loadRentsPending();
  }, [loadRentsPending]);

  const handleAnswerRequest = useCallback(
    async (id, answerRenter) => {
      if (!answerRenter) {
        deuFutSwal
          .fire({
            title: 'Tem certeza de que deseja recusar a solicitação?',
            text: 'Você não poderá reverter isso!',
            imageUrl: exclamationSvg,
            showCloseButton: true,
            showCancelButton: true,
            reverseButtons: true,
            confirmButtonText: 'Sim, Deletar!',
            cancelButtonText: 'Cancelar'.toUpperCase(),
            focusCancel: true,
          })
          .then(async (action) => {
            if (action.value) {
              const { apiCall } = answerRent();

              try {
                await apiCall({ renter_id: id, answer: answerRenter });

                loadRentsPending();

                toast.success('Solicitação recusada com sucesso!');
              } catch (err) {
                toast.error(
                  'Falha ao recusar solicitação, tente novamente mais tarde...'
                );
              }
            }
          });
      } else {
        const { apiCall } = answerRent();

        try {
          await apiCall({ renter_id: id, answer: answerRenter });

          loadRentsPending();

          toast.success('Solicitação aceita com sucesso!');
        } catch (err) {
          toast.error(
            'Falha ao aceitar solicitação, tente novamente mais tarde...'
          );
        }
      }
    },
    [] //eslint-disable-line
  );

  return (
    <Container>
      <Header>
        <TitleHeader>Aluguéis Pendentes</TitleHeader>
      </Header>
      {loading && (
        <div className="loading">
          <BoxLoading loading={loading} />
        </div>
      )}
      {!loading && rents.length > 0 && (
        <Wrapper>
          {rents.map((rent) => {
            const dateStart = new Date(rent.rent.date_start);
            const dateEnd = new Date(rent.rent.date_end);

            return (
              <BoxItem key={rent.id}>
                <BoxField>
                  <Category>Nome do campo: </Category>
                  <ReponseCategory>{rent.footballfield.name}</ReponseCategory>
                </BoxField>
                <BoxField>
                  <Category>Time solicitante: </Category>
                  <ReponseCategory>{rent.team.name}</ReponseCategory>
                </BoxField>
                <BoxField>
                  <Category>Data: </Category>
                  <ReponseCategory>
                    {dateStart.getUTCDate() <= 9
                      ? `0${dateStart.getUTCDate()}`
                      : dateStart.getUTCDate()}
                    /
                    {dateStart.getUTCMonth() + 1 <= 9
                      ? `0${dateStart.getUTCMonth() + 1}`
                      : dateStart.getUTCMonth() + 1}
                    /{dateStart.getUTCFullYear()}
                  </ReponseCategory>
                </BoxField>
                <BoxField className="time">
                  <Category>Inicio: </Category>
                  <ReponseCategory>
                    {dateStart.getHours() < 10
                      ? `0${dateStart.getHours()}`
                      : dateStart.getHours()}
                    :{String(dateStart.getMinutes()).padStart(2, '0')}
                  </ReponseCategory>
                </BoxField>
                <BoxField className="time">
                  <Category>Término: </Category>
                  <ReponseCategory>
                    {dateEnd.getHours() < 10
                      ? `0${dateEnd.getHours()}`
                      : dateEnd.getHours()}
                    :{String(dateEnd.getMinutes()).padStart(2, '0')}
                  </ReponseCategory>
                </BoxField>
                <BoxField className="buttons">
                  <FontAwesomeIcon
                    id="refuse"
                    icon={faTimes}
                    color="#F32525"
                    size={22}
                    onClick={() => handleAnswerRequest(rent.id, false)}
                  />
                  <FontAwesomeIcon
                    icon={faCheck}
                    color="#3eb400"
                    size={22}
                    onClick={() => handleAnswerRequest(rent.id, true)}
                  />
                </BoxField>
              </BoxItem>
            );
          })}
        </Wrapper>
      )}
      {!loading && rents.length === 0 && (
        <FieldsEmpty>
          <span>Nenhum aluguel pendente...</span>
          <FontAwesomeIcon icon={faSadTear} />
        </FieldsEmpty>
      )}
    </Container>
  );
}

export default Requests;
