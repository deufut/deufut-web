import styled, { keyframes } from 'styled-components';

const forward = keyframes`
0% {
  margin-left: 0;
} 50% {
  margin-left: 15px;
} 100% {
  margin-left: 0;
}
`;

export const Wrapper = styled.div`
  width: 100%;
  height: 100vh;

  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
`;

export const SectionSignIn = styled.section`
  width: 100%;
  max-width: 500px;
  padding: 20px;
  background: #f0f0f5;
  border-radius: 30px;
  display: flex;
  flex-direction: column;
  position: absolute;

  img {
    height: 100%;
    max-height: 300px;
    width: 100%;
  }

  form {
    display: flex;
    flex-direction: column;

    div.box-input {
      display: flex;
      align-items: center;
      justify-content: center;
      position: relative;
      flex-direction: column;

      span {
        margin-bottom: 10px;
        margin-left: 6px;
        font-size: 13px;
        color: #f64c75;
        align-self: flex-start;
        font-weight: bold;
        animation: ${forward} 500ms ease-in-out;
      }

      input {
        width: 100%;
        resize: vertical;
        border: 1px solid #dcdce6;
        border-radius: 8px;
        padding: 16px 24px;
        margin-bottom: 10px;
      }

      svg {
        position: absolute;
        top: 16px;
        right: 10px;
        color: #3eb400;
      }
    }

    div.box-button {
      width: 100%;
      margin-top: 15px;

      display: flex;
      justify-content: flex-end;
      align-items: center;
    }

    a.link-forgot {
      display: flex;
      align-items: center;
      justify-content: flex-end;
      margin-bottom: 14px;
      color: #41414d;
      font-size: 12px;
      font-weight: 500;
      transition: opacity 0.2s;

      &:hover {
        opacity: 0.8;
      }
    }

    div.box-link {
      display: flex;
      align-items: center;
      justify-content: flex-end;

      a {
        display: flex;
        align-items: center;
        justify-content: flex-end;
        margin-top: 23px;
        color: #41414d;
        width: 26%;
        font-size: 12px;
        font-weight: 500;
        transition: opacity 0.2s;

        &:hover {
          opacity: 0.8;
        }
      }
    }
  }
`;

export const ButtonSubmit = styled.button`
  width: 100%;
  height: 45px;
  background: #3eb400;
  border: 0;
  border-radius: 8px;
  color: #fff;
  cursor: pointer;
  font-weight: 700;
  display: inline-block;
  text-align: center;
  text-decoration: none;
  font-size: 14px;
  line-height: 30px;
  transition: filter 0.2s;
`;

export const Title = styled.span`
  font-size: 26px;
  margin-bottom: 32px;
`;

export const BoxImageBackground = styled.div`
  width: 100%;
  text-align: center;
  opacity: 0.8;
  max-height: 100%;
  overflow: hidden;

  img {
    width: 100%;
    max-height: 100%;
    object-fit: contain;
  }
`;
