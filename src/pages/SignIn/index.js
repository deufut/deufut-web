import React, { useEffect, useState, useCallback } from 'react';

import { Input, Form } from '@rocketseat/unform';

import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faUser,
  faLock,
  faSpinner,
  faEye,
  faEyeSlash,
} from '@fortawesome/free-solid-svg-icons';
import * as Yup from 'yup';
import { useDispatch, useSelector } from 'react-redux';
import logoDeuFut from '../../assets/images/logoDeuFut.svg';
import imageBackground from '../../assets/images/campoBackground.jpg';

import {
  Wrapper,
  SectionSignIn,
  ButtonSubmit,
  Title,
  BoxImageBackground,
} from './styles';
import { signInRequest, setLoading } from '~/store/modules/auth/actions';

const schema = Yup.object().shape({
  email: Yup.string()
    .email('Insira um e-mail válido')
    .required('O e-mail é obrigatório'),
  password: Yup.string().required('A senha é obrigatória'),
});

export default function Login() {
  const [showPassword, setShowPassword] = useState(false);
  const [showEye, setShowEye] = useState(false);

  const dispatch = useDispatch();
  const loading = useSelector((state) => state.auth.loading);

  useEffect(() => {
    dispatch(setLoading(false));
  }, []); //eslint-disable-line

  function signInUser({ email, password }) {
    dispatch(signInRequest(email, password));
  }

  const handleShowEye = useCallback(
    (e) => {
      if (e.target.value !== '') {
        setShowEye(true);
      } else {
        setShowEye(false);
        setShowPassword(false);
      }
    },
    [setShowEye]
  );

  function handleShowPassword() {
    setShowPassword(!showPassword);
  }

  return (
    <Wrapper>
      <BoxImageBackground>
        <img src={imageBackground} alt="" />
      </BoxImageBackground>

      <SectionSignIn>
        <img src={logoDeuFut} alt="DeuFut" />

        <Form schema={schema} onSubmit={signInUser}>
          <Title>Faça seu login</Title>
          <div className="box-input">
            <Input placeholder="E-mail" name="email" type="text" />
            <FontAwesomeIcon icon={faUser} />
          </div>
          <div className="box-input">
            <Input
              onChange={(e) => handleShowEye(e)}
              placeholder="Senha"
              name="password"
              type={showPassword ? 'text' : 'password'}
            />
            {showEye ? (
              <FontAwesomeIcon
                style={{ cursor: 'pointer' }}
                icon={showPassword ? faEye : faEyeSlash}
                onClick={handleShowPassword}
              />
            ) : (
              <FontAwesomeIcon icon={faLock} />
            )}
          </div>

          <div className="box-button">
            <ButtonSubmit type="submit">
              {loading ? <FontAwesomeIcon icon={faSpinner} spin /> : 'Entrar'}
            </ButtonSubmit>
          </div>

          <div className="box-link">
            <Link className="link-register" to="/register">
              Não possui cadastro?
            </Link>
          </div>
        </Form>
      </SectionSignIn>
    </Wrapper>
  );
}
