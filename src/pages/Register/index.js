import React, { useState, useEffect } from 'react';

import { Input, Form } from '@rocketseat/unform';
import { useHistory, Link } from 'react-router-dom';
import { toast } from 'react-toastify';

import * as Yup from 'yup';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import logoDeuFut from '../../assets/images/logoDeuFut.svg';
import imageBackground from '../../assets/images/campoBackground.jpg';
import {
  Wrapper,
  SectionRegister,
  ButtonSubmit,
  Title,
  BoxImageBackground,
  Header,
} from './styles';
import { signUp } from '../../services/endpoints/user';
import { deuFutSwal } from '../../components/global/deuFutSwal/deuFutSwal';

const schema = Yup.object().shape({
  name: Yup.string().min(3, 'Sem abreviações').required('O nome é obrigatório'),
  email: Yup.string()
    .email('Insira um e-mail válido')
    .required('O e-mail é obrigatório'),
  confirmEmail: Yup.string()
    .email('Insert a valid e-mail')
    .required('Confirmação de e-mail é obrigatória')
    .oneOf([Yup.ref('email')], 'E-mail não corresponde'),
  password: Yup.string().required('A senha é obrigatória'),
  phone: Yup.string()
    .min(11, 'Número de celular inválido!! (DDD + 9 + xxxxxxxx')
    .required('O número de celular é obrigatório'),
});

let signal = false;

export default function Register() {
  const [loading, setLoading] = useState(false);
  const history = useHistory();

  async function handleRegisterUser(
    { email, name, password, phone },
    { resetForm }
  ) {
    const { source, apiCall } = signUp();
    try {
      setLoading(true);
      signal = source;
      await apiCall({
        name,
        email,
        password,
        phone,
      });

      setLoading(false);
      deuFutSwal
        .fire({
          icon: 'success',
          text:
            'Obrigado por se cadastrar. Em breve entraremos em contato para você efetuar a confirmação de e-mail, por enquanto, faça login e conheça nossa plataforma!!',
        })
        .then(() => {
          history.push('/');
          resetForm({});
        });
    } catch (err) {
      toast.error('Falha ao efetuar cadastro, tente novamente mais tarde...');
      setLoading(false);
    }
  }

  useEffect(() => {
    return () => {
      if (signal) {
        signal.cancel('Requisição Cancelada');
      }
    };
  }, []);

  return (
    <Wrapper>
      <BoxImageBackground>
        <img src={imageBackground} alt="" />
      </BoxImageBackground>

      <SectionRegister>
        <Header>
          <img src={logoDeuFut} alt="DeuFut" />
          <Title>Faça seu cadastro</Title>
        </Header>

        <Form schema={schema} onSubmit={handleRegisterUser}>
          <Input placeholder="Nome" name="name" type="text" />

          <Input
            placeholder="Número de celular"
            name="phone"
            type="text"
            maxLength={11}
          />

          <Input placeholder="E-mail" name="email" type="email" />

          <Input
            placeholder="Confirmação de e-mail"
            name="confirmEmail"
            type="email"
          />

          <Input placeholder="Senha" name="password" type="password" />
          <div className="box-button">
            <ButtonSubmit type="submit">
              {loading ? (
                <FontAwesomeIcon icon={faSpinner} spin />
              ) : (
                'Cadastrar'
              )}
            </ButtonSubmit>
          </div>
          <div className="box-link">
            <Link to="/">Já tenho cadastro</Link>
          </div>
        </Form>
      </SectionRegister>
    </Wrapper>
  );
}
