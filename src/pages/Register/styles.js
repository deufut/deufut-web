import styled, { keyframes } from 'styled-components';
import DateSelector from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

const forward = keyframes`
0% {
  margin-left: 0;
} 50% {
  margin-left: 15px;
} 100% {
  margin-left: 0;
}
`;

export const Wrapper = styled.div`
  width: 100%;
  height: 100vh;

  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
`;

export const SectionRegister = styled.section`
  width: 100%;
  max-width: 500px;
  padding: 20px;
  background: #f0f0f5;
  border-radius: 30px;
  display: flex;
  flex-direction: column;
  position: absolute;

  form {
    margin-top: 20px;
    display: flex;
    flex-direction: column;

    span {
      margin-bottom: 10px;
      margin-left: 6px;
      font-size: 13px;
      color: #f64c75;
      align-self: flex-start;
      font-weight: bold;
      animation: ${forward} 500ms ease-in-out;
    }

    input {
      width: 100%;
      resize: vertical;
      border: 1px solid #dcdce6;
      border-radius: 8px;
      padding: 16px 24px;
      margin-bottom: 10px;
    }

    div.box-button {
      width: 100%;
      display: flex;
      justify-content: flex-end;
      align-items: center;
    }

    div.box-inputs {
      width: 100%;
      display: flex;
      justify-content: space-between;

      input.date {
        width: 55%;
        resize: vertical;
        border: 1px solid #dcdce6;
        border-radius: 8px;
        padding: 16px 24px;
        margin-bottom: 10px;
      }

      input.phone {
        width: 40%;
        resize: vertical;
        border: 1px solid #dcdce6;
        border-radius: 8px;
        padding: 16px 24px;
        margin-bottom: 10px;
      }
    }

    div.box-link {
      display: flex;
      align-items: center;
      justify-content: flex-end;

      a {
        display: flex;
        align-items: center;
        justify-content: flex-end;
        margin-top: 23px;
        color: #41414d;
        width: 21%;
        font-size: 12px;
        font-weight: 500;
        transition: opacity 0.2s;

        &:hover {
          opacity: 0.8;
        }
      }
    }
  }
`;

export const ButtonSubmit = styled.button`
  width: 100%;
  height: 45px;
  background: #3eb400;
  border: 0;
  border-radius: 8px;
  color: #fff;
  cursor: pointer;
  font-weight: 700;
  display: inline-block;
  text-align: center;
  text-decoration: none;
  font-size: 14px;
  line-height: 30px;
  transition: filter 0.2s;
`;

export const Title = styled.span`
  font-size: 26px;
  margin-left: 22px;
`;

export const BoxImageBackground = styled.div`
  width: 100%;
  text-align: center;
  opacity: 0.8;
  max-height: 100%;
  overflow: hidden;

  img {
    width: 100%;
    max-height: 100%;
    object-fit: contain;
  }
`;

export const Header = styled.div`
  display: flex;
  align-items: center;

  img {
    width: 17%;
  }
`;

export const DatePicker = styled(DateSelector)`
  height: 50px;
  width: 100%;
  border: 0;
  border-radius: 24px;
  // background: #f0f2f7;
  padding-left: 15px;
  color: #a6b1c3;

  @media (max-width: 575px) {
    height: 40px;
    font-size: 14px;
  }
`;
