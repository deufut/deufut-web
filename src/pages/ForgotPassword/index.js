import React from 'react';

import { Input, Form } from '@rocketseat/unform';

import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';
import * as Yup from 'yup';
import logoDeuFut from '../../assets/images/logoDeuFut.svg';
import imageBackground from '../../assets/images/campoBackground.jpg';

import {
  Wrapper,
  SectionSignIn,
  ButtonSubmit,
  Title,
  BoxImageBackground,
} from './styles';

const schema = Yup.object().shape({
  email: Yup.string()
    .email('Insira um e-mail válido')
    .required('O e-mail é obrigatório'),
});

function handleNewPassword() {
  // console.log('teste');
}
export default function ForgotPassword() {
  return (
    <Wrapper>
      <BoxImageBackground>
        <img src={imageBackground} alt="" />
      </BoxImageBackground>

      <SectionSignIn>
        <img src={logoDeuFut} alt="DeuFut" />

        <Form schema={schema} onSubmit={handleNewPassword}>
          <Title>Informe seu e-mail</Title>
          <div className="box-input">
            <Input placeholder="E-mail" name="email" type="text" />
            <FontAwesomeIcon icon={faEnvelope} />
          </div>
          <div className="box-button">
            <ButtonSubmit type="submit">Recuperar senha</ButtonSubmit>
          </div>

          <div className="box-link">
            <Link className="link-remember" to="/">
              Lembrei minha senha!
            </Link>
          </div>
        </Form>
      </SectionSignIn>
    </Wrapper>
  );
}
