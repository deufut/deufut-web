import React, { useCallback, useEffect, useState } from 'react';

import { Input, Form } from '@rocketseat/unform';
import * as Yup from 'yup';

import { toast } from 'react-toastify';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft, faSpinner } from '@fortawesome/free-solid-svg-icons';
import { useParams } from 'react-router-dom';
import { editField, getFieldById } from '~/services/endpoints/fields';

import {
  Container,
  Wrapper,
  HeaderBox,
  FieldRegister,
  InfoRow,
  FieldWrapper,
  TextDescription,
  BoxButton,
} from './styles';
import { deuFutSwal } from '~/components/global/deuFutSwal/deuFutSwal';
import history from '~/services/history';

const schema = Yup.object().shape({
  name: Yup.string().required('O nome do campo é obrigatório'),
  phone: Yup.string()
    .min(11, 'Número de celular inválido!! (DDD + 9 + xxxxxxxx')
    .required('O número de celular é obrigatório'),
  cnpj: Yup.string()
    .min(14, 'CNPJ inválido, mínimo de 14 dígitos')
    .required('O CNPJ é obrigatório'),
  address: Yup.string().required('O endereço é obrigatório'),
  cep: Yup.string()
    .min(8, 'O CEP é inválido, mínimo de 8 dígitos')
    .required('O CEP é obrigatório'),
  state: Yup.string().required('O Estado é obrigatório'),
  city: Yup.string().required('A cidade é obrigatória'),
  number: Yup.string().required(
    'O número do local é obrigatório, caso não tenha, coloque 0!!'
  ),
  neighborhood: Yup.string().required('O bairro é obrigatório'),
  description: Yup.string().required('A descrição é obrigatória'),
});

let signal = false;

function EditField() {
  const [loading, setLoading] = useState(false);
  const [loadingField, setLoadingField] = useState(false);
  const [value, setValue] = useState([]);

  const { fieldId } = useParams();

  async function handleEditField() {
    const { source, apiCall } = editField();
    try {
      setLoading(true);

      signal = source;

      await apiCall({
        id: value.id,
        name: value.name,
        phone: value.phone,
        cnpj: value.cnpj,
        address: value.address,
        cep: value.cep,
        state: value.state,
        city: value.city,
        number: value.number,
        neighborhood: value.neighborhood,
        complement: value.complement,
        description: value.description,
      });

      setLoading(false);
      deuFutSwal.fire({
        icon: 'success',
        text: 'Campo editado com sucesso!!',
      });
    } catch (err) {
      setLoading(false);
      toast.error('Falha ao editar campo, tente novamente mais tarde...');
    }
  }

  async function loadField() {
    const { apiCall, source } = getFieldById();

    try {
      signal = source;
      setLoadingField(true);
      const response = await apiCall({ fieldId });

      setLoadingField(false);
      setValue(response.data);
    } catch (err) {
      setLoadingField(false);
      toast.error(
        'Falha ao carregar os dados do campo, tente novamente mais tarde...'
      );
    }
  }

  useEffect(() => {
    loadField();

    return () => {
      if (signal) {
        signal.cancel('Requisição Cancelada');
      }
    };
  }, []); //eslint-disable-line

  const handleChange = useCallback(
    (e) => setValue({ ...value, [e.target.name]: e.target.value }),
    [value]
  );

  return (
    <Container>
      <Wrapper>
        <HeaderBox>
          <FontAwesomeIcon
            onClick={() => {
              history.goBack();
            }}
            icon={faArrowLeft}
          />
          <span>Editar campo</span>
        </HeaderBox>
        <FieldRegister>
          <Form schema={schema} onSubmit={handleEditField}>
            <InfoRow>
              <FieldWrapper wd={40}>
                <Input
                  placeholder="Nome"
                  type="text"
                  name="name"
                  value={value.name ? value.name : ''}
                  onChange={handleChange}
                  disabled={loadingField}
                />
              </FieldWrapper>

              <FieldWrapper wd={20}>
                <Input
                  placeholder="Celular"
                  type="text"
                  name="phone"
                  value={value.phone ? value.phone : ''}
                  onChange={handleChange}
                  disabled={loadingField}
                />
              </FieldWrapper>

              <FieldWrapper wd={40}>
                <Input
                  placeholder="CNPJ"
                  type="text"
                  name="cnpj"
                  value={value.cnpj ? value.cnpj : ''}
                  onChange={handleChange}
                  disabled={loadingField}
                />
              </FieldWrapper>

              <FieldWrapper wd={100}>
                <Input
                  placeholder="Endereço"
                  type="text"
                  name="address"
                  value={value.address ? value.address : ''}
                  onChange={handleChange}
                  disabled={loadingField}
                />
              </FieldWrapper>

              <FieldWrapper wd={25}>
                <Input
                  placeholder="CEP"
                  type="text"
                  name="cep"
                  value={value.cep ? value.cep : ''}
                  onChange={handleChange}
                  disabled={loadingField}
                />
              </FieldWrapper>

              <FieldWrapper wd={25}>
                <Input
                  placeholder="Estado"
                  type="text"
                  name="state"
                  value={value.state ? value.state : ''}
                  onChange={handleChange}
                  disabled={loadingField}
                />
              </FieldWrapper>

              <FieldWrapper wd={25}>
                <Input
                  placeholder="Cidade"
                  type="text"
                  name="city"
                  value={value.city ? value.city : ''}
                  onChange={handleChange}
                  disabled={loadingField}
                />
              </FieldWrapper>

              <FieldWrapper wd={25}>
                <Input
                  placeholder="Número"
                  type="text"
                  name="number"
                  value={value.number ? value.number : ''}
                  onChange={handleChange}
                  disabled={loadingField}
                />
              </FieldWrapper>

              <FieldWrapper wd={50}>
                <Input
                  placeholder="Bairro"
                  type="text"
                  name="neighborhood"
                  value={value.neighborhood ? value.neighborhood : ''}
                  onChange={handleChange}
                  disabled={loadingField}
                />
              </FieldWrapper>

              <FieldWrapper wd={50}>
                <Input
                  placeholder="Complemento (Não é obrigatório)"
                  type="text"
                  name="complement"
                  value={value.complement ? value.complement : ''}
                  onChange={handleChange}
                  disabled={loadingField}
                />
              </FieldWrapper>

              <FieldWrapper wd={100}>
                <TextDescription
                  multiline
                  name="description"
                  placeholde="Descrição do campo"
                  value={value.description ? value.description : ''}
                  onChange={handleChange}
                  disabled={loadingField}
                />
              </FieldWrapper>
            </InfoRow>
            <BoxButton>
              <button type="submit">
                {loading ? <FontAwesomeIcon icon={faSpinner} spin /> : 'Editar'}
              </button>
            </BoxButton>
          </Form>
        </FieldRegister>
      </Wrapper>
    </Container>
  );
}

export default EditField;
