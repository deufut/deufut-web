import React, { useEffect, useState } from 'react';

import { Input, Form } from '@rocketseat/unform';
import * as Yup from 'yup';

import { toast } from 'react-toastify';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft, faSpinner } from '@fortawesome/free-solid-svg-icons';
import { registerField } from '~/services/endpoints/fields';

import {
  Container,
  Wrapper,
  HeaderBox,
  FieldRegister,
  InfoRow,
  FieldWrapper,
  TextDescription,
  BoxButton,
} from './styles';
import { deuFutSwal } from '~/components/global/deuFutSwal/deuFutSwal';
import history from '~/services/history';

const schema = Yup.object().shape({
  name: Yup.string().required('O nome do campo é obrigatório'),
  phone: Yup.string()
    .min(11, 'Número de celular inválido!! (DDD + 9 + xxxxxxxx')
    .required('O número de celular é obrigatório'),
  cnpj: Yup.string()
    .min(14, 'CNPJ inválido, mínimo de 14 dígitos')
    .required('O CNPJ é obrigatório'),
  address: Yup.string().required('O endereço é obrigatório'),
  cep: Yup.string()
    .min(8, 'O CEP é inválido, mínimo de 8 dígitos')
    .required('O CEP é obrigatório'),
  state: Yup.string().required('O Estado é obrigatório'),
  city: Yup.string().required('A cidade é obrigatória'),
  number: Yup.string().required(
    'O número do local é obrigatório, caso não tenha, coloque 0!!'
  ),
  neighborhood: Yup.string().required('O bairro é obrigatório'),
  description: Yup.string().required('A descrição é obrigatória'),
});

let signal = false;

function CreateField() {
  const [loading, setLoading] = useState(false);

  async function handleRegisterField(
    {
      name,
      phone,
      cnpj,
      address,
      cep,
      state,
      city,
      number,
      neighborhood,
      complement,
      description,
    },
    { resetForm }
  ) {
    const { source, apiCall } = registerField();
    try {
      setLoading(true);

      signal = source;
      await apiCall({
        name,
        phone,
        cnpj,
        address,
        cep,
        state,
        city,
        number,
        neighborhood,
        complement,
        description,
      });

      setLoading(false);
      deuFutSwal
        .fire({
          icon: 'success',
          text:
            'Obrigado por cadastrar o seu campo, agora basta aguardar para receber as solicitações de locação!!',
        })
        .then(() => {
          history.push('/dashboard');
          resetForm({});
        });
    } catch (err) {
      setLoading(false);
      toast.error('Falha ao cadastrar campo, tente novamente mais tarde...');
    }
  }

  useEffect(() => {
    return () => {
      if (signal) {
        signal.cancel('Requisição Cancelada');
      }
    };
  }, []);

  return (
    <Container>
      <Wrapper>
        <HeaderBox>
          <FontAwesomeIcon
            onClick={() => {
              history.goBack();
            }}
            icon={faArrowLeft}
          />
          <span>Adicione seu campo</span>
        </HeaderBox>
        <FieldRegister>
          <Form schema={schema} onSubmit={handleRegisterField}>
            <InfoRow>
              <FieldWrapper wd={40}>
                <Input placeholder="Nome" type="text" name="name" />
              </FieldWrapper>

              <FieldWrapper wd={20}>
                <Input placeholder="Celular" type="text" name="phone" />
              </FieldWrapper>

              <FieldWrapper wd={40}>
                <Input placeholder="CNPJ" type="text" name="cnpj" />
              </FieldWrapper>

              <FieldWrapper wd={100}>
                <Input placeholder="Endereço" type="text" name="address" />
              </FieldWrapper>

              <FieldWrapper wd={25}>
                <Input placeholder="CEP" type="text" name="cep" />
              </FieldWrapper>

              <FieldWrapper wd={25}>
                <Input placeholder="Estado" type="text" name="state" />
              </FieldWrapper>

              <FieldWrapper wd={25}>
                <Input placeholder="Cidade" type="text" name="city" />
              </FieldWrapper>

              <FieldWrapper wd={25}>
                <Input placeholder="Número" type="text" name="number" />
              </FieldWrapper>

              <FieldWrapper wd={50}>
                <Input placeholder="Bairro" type="text" name="neighborhood" />
              </FieldWrapper>

              <FieldWrapper wd={50}>
                <Input
                  placeholder="Complemento (Não é obrigatório)"
                  type="text"
                  name="complement"
                />
              </FieldWrapper>

              <FieldWrapper wd={100}>
                <TextDescription
                  multiline
                  name="description"
                  placeholde="Descrição do campo"
                />
              </FieldWrapper>
            </InfoRow>
            <BoxButton>
              <button type="submit">
                {loading ? (
                  <FontAwesomeIcon icon={faSpinner} spin />
                ) : (
                  'Cadastrar'
                )}
              </button>
            </BoxButton>
          </Form>
        </FieldRegister>
      </Wrapper>
    </Container>
  );
}

export default CreateField;
