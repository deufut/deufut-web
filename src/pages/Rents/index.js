import {
  faArrowLeft,
  faPencilAlt,
  faSadTear,
  faTimes,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useCallback, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { deuFutSwal } from '~/components/global/deuFutSwal/deuFutSwal';
import Pagination from '~/components/global/Pagination';
import {
  getRents,
  deleteRent,
  registerRent,
  editRent,
} from '~/services/endpoints/rents';
import exclamationSvg from '~/assets/icons/exclamation-mark.svg';

import {
  Container,
  Wrapper,
  HeaderBox,
  BoxRight,
  RentListing,
  BoxRents,
  BoxTitles,
  FieldsEmpty,
  Button,
} from './styles';
import BoxLoading from '~/components/global/BoxLoading';
import history from '~/services/history';
import ModalCenter from '~/components/global/ModalCenter';
import RentModals from '~/components/rents/RentModals';

let signal = false;

function Rents() {
  const { fieldId } = useParams();

  const [recordsTotal, setRecordsTotal] = useState(0);
  const [loading, setLoading] = useState(false);
  const [totalPage, setTotalPage] = useState(0);
  const [refresh, setRefresh] = useState(false);
  const [page, setPage] = useState(1);
  const [rents, setRents] = useState([]);
  const [nameField, setNameField] = useState('');
  const [isOpen, setIsOpen] = useState(false);
  const [rentId, setRentId] = useState('');
  const [loadingCreate, setLoadingCreate] = useState(false);
  const [loadingEdit, setLoadingEdit] = useState(false);

  const loadRents = useCallback(() => {
    const { source, apiCall } = getRents();

    const pageLength = 6;
    signal && signal.cancel();
    signal = source;
    setLoading(true);

    apiCall({
      footballfield_id: parseInt(fieldId, 10),
      start: (page - 1) * pageLength,
      length: pageLength,
    })
      .then((response) => {
        const { data, records } = response.data;

        if (records >= pageLength && !data.rents.length && page > 1) {
          setPage((old) => old - 1);
          return;
        }

        setNameField(data.footballfield.name);
        setRecordsTotal(records);
        setRents(data.rents);
        setLoading(false);
        setTotalPage(Math.ceil(records / pageLength));
      })
      .catch((err) => {
        if (err.response) {
          setLoading(false);
          toast.error(
            'Erro ao carregar aluguéis, tente novamente mais tarde...'
          );
        }
      });
  }, [page, fieldId]);

  useEffect(() => {
    loadRents();
  }, [loadRents, page, refresh]);

  const handleChangePage = useCallback((param) => {
    setPage(param);
  }, []);

  const handleModal = useCallback(() => {
    setIsOpen(!isOpen);
    setRentId('');
  }, [isOpen]);

  function handleDeleteRent(id) {
    deuFutSwal
      .fire({
        title: 'Tem certeza de que deseja excluir?',
        text: 'Você não poderá reverter isso!',
        imageUrl: exclamationSvg,
        showCloseButton: true,
        showCancelButton: true,
        reverseButtons: true,
        confirmButtonText: 'Sim, Deletar!',
        cancelButtonText: 'Cancelar'.toUpperCase(),
        focusCancel: true,
      })
      .then(async (action) => {
        if (action.value) {
          const { apiCall } = deleteRent();

          await apiCall({ rentId: id })
            .then(() => {
              setRefresh(!refresh);
              toast.success('Aluguel deletado com sucesso!');
            })
            .catch(() => {
              toast.error(
                'Erro ao deletar aluguel, tente novamente mais tarde...'
              );
            });
        }
      });
  }

  const handleCreateRent = useCallback(
    async ({ time_start, time_end, value, date }) => {
      const valueRent = parseFloat(value.toString().replace(',', '.'));
      const dStart = new Date(
        date.substring(0, 4),
        date.substring(5, 7) - 1,
        date.substring(8, 10) - 1,
        time_start.substring(0, 2),
        time_start.substring(3, 5),
        0,
        0
      );
      const dEnd = new Date(
        date.substring(0, 4),
        date.substring(5, 7),
        date.substring(8, 10),
        time_end.substring(0, 2),
        time_end.substring(3, 5),
        0,
        0
      );

      const { apiCall } = registerRent();
      try {
        setLoadingCreate(true);
        await apiCall({
          footballfield_id: fieldId,
          date_start: dStart,
          date_end: dEnd,
          value: valueRent,
        });

        setLoadingCreate(false);
        toast.success('Aluguel criado com sucesso!');
        setIsOpen(false);
        loadRents();
      } catch (err) {
        if (err.response) {
          setLoadingCreate(false);
          toast.error('Erro ao criar aluguel, tente novamente mais tarde!');
        }
      }
    },
    [loadRents] //eslint-disable-line
  );

  const handleEditRent = useCallback(
    async ({ time_start, time_end, value, date }) => {
      const { apiCall } = editRent();
      const valueRent = parseFloat(value.toString().replace(',', '.'));
      const dStart = new Date(
        date.substring(0, 4),
        date.substring(5, 7) - 1,
        date.substring(8, 10) - 1,
        time_start.substring(0, 2),
        time_start.substring(3, 5),
        0,
        0
      );
      const dEnd = new Date(
        date.substring(0, 4),
        date.substring(5, 7),
        date.substring(8, 10),
        time_end.substring(0, 2),
        time_end.substring(3, 5),
        0,
        0
      );
      try {
        setLoadingEdit(true);
        await apiCall({
          rent_id: rentId,
          date_start: dStart,
          date_end: dEnd,
          value: valueRent,
        });

        setLoadingEdit(false);
        toast.success('Campo editado com sucesso!');
        setIsOpen(false);
        loadRents();
      } catch (err) {
        if (err.response) {
          setLoadingEdit(false);
          toast.error('Falha ao editar campo, tente novamente mais tarde...');
        }
      }
    },
    [loadRents, rentId]
  );

  const sideEffectOpenModal = useCallback(
    (id) => {
      setRentId(id);
      setIsOpen(!isOpen);
    },
    [isOpen]
  );

  const RenderRents = () => {
    if (rents.length > 0) {
      return (
        <>
          {rents.map((rent) => {
            const dateStart = new Date(rent.date_start);
            const dateEnd = new Date(rent.date_end);

            return (
              <>
                <BoxRents key={rent.id}>
                  <span className="date">
                    {dateStart.getUTCDate() <= 9
                      ? `0${dateStart.getUTCDate()}`
                      : dateStart.getUTCDate()}
                    /
                    {dateStart.getUTCMonth() + 1 <= 9
                      ? `0${dateStart.getUTCMonth() + 1}`
                      : dateStart.getUTCMonth() + 1}
                    /{dateStart.getUTCFullYear()}
                  </span>
                  <div>
                    <span className="time-start">
                      Inicio:
                      {dateStart.getHours() < 10
                        ? `0${dateStart.getHours()}`
                        : dateStart.getHours()}
                      :{String(dateStart.getMinutes()).padStart(2, '0')}
                    </span>
                    <span className="time-end">
                      Término:
                      {dateEnd.getHours() < 10
                        ? `0${dateEnd.getHours()}`
                        : dateEnd.getHours()}
                      :{String(dateEnd.getMinutes()).padStart(2, '0')}
                    </span>
                  </div>
                  <span className="value">
                    R$ {String(rent.value.toFixed(2)).replace('.', ',')}
                  </span>
                  <span className="status" status={rent.reserved}>
                    {rent.reserved ? 'Reservado' : 'Disponível'}
                  </span>
                  <FontAwesomeIcon
                    icon={faPencilAlt}
                    onClick={() => sideEffectOpenModal(rent.id)}
                    className="edit"
                  />
                  <FontAwesomeIcon
                    icon={faTimes}
                    onClick={() => handleDeleteRent(rent.id)}
                    className="delete"
                  />
                </BoxRents>
              </>
            );
          })}
        </>
      );
    }

    return (
      <FieldsEmpty>
        <span>Nenhum aluguel cadastrado...</span>
        <FontAwesomeIcon icon={faSadTear} />
      </FieldsEmpty>
    );
  };

  return (
    <Container>
      <Wrapper>
        {!loading && (
          <HeaderBox>
            <BoxRight>
              <FontAwesomeIcon
                onClick={() => {
                  history.goBack();
                }}
                icon={faArrowLeft}
              />

              <span>{nameField} -</span>
              <span> {`Seus aluguéis (${recordsTotal})`}</span>
            </BoxRight>
            <Button type="button" onClick={() => setIsOpen(!isOpen)}>
              Adicionar aluguel
            </Button>
          </HeaderBox>
        )}
        {loading && (
          <div className="wrapper-bottom">
            <BoxLoading loading={loading} />
          </div>
        )}

        {!loading && (
          <RentListing>
            <BoxTitles>
              <span className="title-date">Data</span>
              <span className="title-start">Horário</span>
              <span className="title-end">Valor</span>
              <span className="title-reserved">Status</span>
            </BoxTitles>

            <RenderRents />
          </RentListing>
        )}

        {rents.length > 0 && !loading && (
          <Pagination
            totalPages={totalPage}
            changePage={handleChangePage}
            page={page}
          />
        )}
      </Wrapper>
      {isOpen && (
        <ModalCenter isOpen={isOpen} close={handleModal}>
          <RentModals
            close={handleModal}
            rentId={rentId}
            handleCreate={handleCreateRent}
            handleEdit={handleEditRent}
            loadingCreate={loadingCreate}
            loadingEdit={loadingEdit}
          />
        </ModalCenter>
      )}
    </Container>
  );
}

export default Rents;
