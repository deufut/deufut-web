import React, { useCallback, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faCalendarAlt,
  faPencilAlt,
  faSadTear,
  faSearch,
  faTimes,
} from '@fortawesome/free-solid-svg-icons';

import { toast } from 'react-toastify';
import { debounce } from 'lodash';
import BoxLoading from '~/components/global/BoxLoading';
import exclamationSvg from '~/assets/icons/exclamation-mark.svg';

import {
  Container,
  Wrapper,
  HeaderBox,
  FieldListing,
  BoxFields,
  BoxTitles,
  FieldsEmpty,
  BoxRight,
} from './styles';
import history from '~/services/history';
import { deuFutSwal } from '~/components/global/deuFutSwal/deuFutSwal';
import Pagination from '~/components/global/Pagination';
import { getFields, deleteField } from '~/services/endpoints/fields';
import InputSearch from '~/components/global/InputSearch';

let signal = false;

export default function Dashboard() {
  const [loading, setLoading] = useState(false);
  const [totalPage, setTotalPage] = useState(0);
  const [recordsTotal, setRecordsTotal] = useState(0);
  const [refresh, setRefresh] = useState(false);
  const [page, setPage] = useState({
    pageStart: 1,
    searchValue: '',
  });
  const [fields, setFields] = useState([]);

  const loadingDelete = useSelector((state) => state.field.loadingDelete);

  const loadFields = useCallback(() => {
    const { source, apiCall } = getFields();

    const pageLength = 6;
    signal && signal.cancel();
    signal = source;
    setLoading(true);

    apiCall({
      start: (page.pageStart - 1) * pageLength,
      length: pageLength,
      search: page.searchValue,
    })
      .then((response) => {
        const { data, records } = response.data;

        if (records >= pageLength && !data.length && page.pageStart > 1) {
          setPage((old) => old - 1);
          return;
        }
        setRecordsTotal(records);
        setFields(data);
        setLoading(false);
        setTotalPage(Math.ceil(records / pageLength));
      })
      .catch((err) => {
        if (err.response) {
          setLoading(false);
          toast.error('Erro ao carregar campos, tente novamente mais tarde...');
        }
      });
  }, [page]);

  function handleDeleteField(id) {
    deuFutSwal
      .fire({
        title: 'Tem certeza de que deseja excluir?',
        text: 'Você não poderá reverter isso!',
        imageUrl: exclamationSvg,
        showCloseButton: true,
        showCancelButton: true,
        reverseButtons: true,
        confirmButtonText: 'Sim, Deletar!',
        cancelButtonText: 'Cancelar'.toUpperCase(),
        focusCancel: true,
      })
      .then((action) => {
        if (action.value) {
          const { apiCall } = deleteField();

          apiCall({ fieldId: id })
            .then(() => {
              setRefresh(!refresh);
              toast.success('Campo deletado com sucesso!');
            })
            .catch(() => {
              toast.error(
                'Erro ao deletar campo, tente novamente mais tarde...'
              );
            });
        }
      });
  }

  function handleEditNavigation(id) {
    history.push(`/edit-field/${id}`);
  }

  useEffect(() => {
    loadFields();
  }, [loadFields, page, refresh]);

  const handleChangePage = useCallback((param) => {
    setPage((state) => {
      return { ...state, pageStart: param };
    });
  }, []);

  const handleSearchValue = useCallback((value) => {
    setPage({ pageStart: 1, searchValue: value });
  }, []);

  const debounced = debounce(handleSearchValue, 600);

  return (
    <>
      <Container>
        <Wrapper>
          <HeaderBox>
            <BoxRight>
              <span>{`Seus campos (${recordsTotal})`}</span>
              <InputSearch
                icon={faSearch}
                onChange={(e) => debounced(e.target.value)}
                placeholder="Buscar Por Campos"
              />
            </BoxRight>
            <Link to="/create-field">Adicionar campos</Link>
          </HeaderBox>
          {loading && (
            <div className="wrapper-bottom">
              <BoxLoading loading={loading} />
            </div>
          )}

          {fields.length > 0 && !loading && (
            <FieldListing>
              <BoxTitles>
                <span className="title-name">Nome</span>
                <span className="title-adress">Endereço</span>
                <span className="title-cep">CEP</span>
                <span className="title-neighborhood">Bairro</span>
              </BoxTitles>
              {fields.map(({ id, address, cep, name, neighborhood, phone }) => (
                <BoxFields key={id}>
                  <div>
                    <span className="name-field">{name}</span>
                    <span className="number-field">{phone}</span>
                  </div>
                  <span className="adress">{address}</span>
                  <span className="cep">{cep}</span>
                  <span className="neighborhood">{neighborhood}</span>
                  <FontAwesomeIcon
                    icon={faCalendarAlt}
                    onClick={() => history.push(`/rents/${id}`)}
                    className="new-rent"
                  />
                  <FontAwesomeIcon
                    icon={faPencilAlt}
                    onClick={() => handleEditNavigation(id)}
                    className="edit"
                  />
                  <FontAwesomeIcon
                    icon={faTimes}
                    onClick={() => handleDeleteField(id)}
                    className="delete"
                  />
                </BoxFields>
              ))}
            </FieldListing>
          )}
          {fields.length <= 0 && !loading && (
            <FieldsEmpty>
              <span>Nenhum campo cadastrado...</span>
              <FontAwesomeIcon icon={faSadTear} />
            </FieldsEmpty>
          )}
          {!loading && fields.length !== 0 && (
            <Pagination
              totalPages={totalPage}
              changePage={handleChangePage}
              page={page.pageStart}
              loading={loadingDelete}
            />
          )}
        </Wrapper>
      </Container>
    </>
  );
}
