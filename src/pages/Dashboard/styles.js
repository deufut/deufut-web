import styled from 'styled-components';

export const Container = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const Wrapper = styled.div`
  width: 100%;
  max-width: 1200px;
  height: 100%;
  min-height: 80vh;
  background: #fff;
  border-radius: 25px;
  padding: 10px;

  div.wrapper-bottom {
    height: 60vh;
    display: flex;
    align-items: center;
  }
`;

export const HeaderBox = styled.header`
  padding: 20px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 20px;
  border-bottom: 1px solid #006400;

  a {
    border: 0;
    background: none;
    padding: 10px;
    border-radius: 5px;
    color: #3eb400;
    border: 1px solid #3eb400;
    text-decoration: none;
    cursor: pointer;
  }
`;

export const FieldListing = styled.div`
  min-height: 62vh;
`;

export const BoxTitles = styled.div`
  width: 100%;
  padding: 20px;
  display: flex;

  span.title-name {
    width: 25%;
    font-weight: bold;
    color: #333;
  }

  span.title-adress {
    width: 30%;
    font-weight: bold;
    color: #333;
  }

  span.title-cep {
    width: 15%;
    font-weight: bold;
    color: #333;
  }

  span.title-neighborhood {
    width: 30%;
    margin-right: 40px;
    color: #333;
    font-weight: bold;
  }
`;

export const BoxFields = styled.div`
  padding: 20px;
  border-bottom: 1px solid #eee;
  display: flex;
  align-items: center;
  width: 100%;

  span.adress {
    color: #333;
    width: 30%;
  }

  span.cep {
    color: #333;
    width: 15%;
  }

  span.neighborhood {
    color: #333;
    width: 30%;
  }

  svg.edit {
    color: #3eb400;
    cursor: pointer;
    margin-right: 15px;
  }

  svg.delete {
    color: #cc0000;
    cursor: pointer;
  }

  svg.new-rent {
    color: #00008b;
    margin-right: 15px;
    cursor: pointer;
  }

  div {
    display: flex;
    flex-direction: column;
    width: 25%;

    span.name-field {
      color: #333;
      width: 100%;
      margin-bottom: 10px;
    }

    span.number-field {
      color: #333;
      width: 100%;
    }
  }
`;

export const FieldsEmpty = styled.div`
  height: 60vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  span {
    font-size: 30px;
    font-weight: bold;
    color: #a6b1c3;
  }

  svg {
    font-size: 60px;
    color: #a6b1c3;
    margin-top: 20px;
  }
`;

export const BoxRight = styled.div`
  display: flex;
  align-items: center;

  span {
    color: #333;
    font-size: 20px;
    font-weight: bold;
    margin-right: 40px;
  }
`;
