import { createGlobalStyle } from 'styled-components';
import 'react-toastify/dist/ReactToastify.css';

export const GlobalStyle = createGlobalStyle`
@import url('https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i&display=swap');
@import url('https://fonts.googleapis.com/css?family=Rubik:300,400,700&display=swap');

  * {
    margin: 0;
    padding: 0;
    outline: 0;
    box-sizing: border-box;
  };

  img{
    max-width: 100%;
  };

  html, body, #root {
    height: 100%;
  };

  body {
    text-rendering: optimizeLegibility !important;
    -webkit-font-smoothing: antialiased !important;
    font-family: 'Roboto', sans-serif;
    font-size: 1em;
  };

  a {
    &:hover {
      text-decoration: none;
    };

    &[disabled] {
      cursor: not-allowed;
      &:active {
        pointer-events: none;
        box-shadow: none;
      };
    };
  };

  button {
    border: 0;
    background: none;

    &:focus {
      outline:none;
    };
    &[disabled] {
      cursor: not-allowed;
      &:active {
        box-shadow: none;
      };
    };
  };

  ul {
    list-style: none;
    padding: 0;
    margin: 0;
  };

  /* Sweet Alert2 Styles */
  h2.swal2-title {
    color: #1F2D50 !important;
    font-weight: 500 !important;
    word-break: break-word;
  };

  div.swal2-content {
    color: #1F2D50 !important;
    /* font-weight: 500 !important; */

    &.pre-wrap {
      white-space: pre-wrap;
    }
  };

  div.swal2-actions {
    margin-bottom: 4%;
    display: flex !important;
    flex-wrap: nowrap ;

    @media (max-width: 400px) {
      flex-wrap: wrap;
    };
  };

  button.swal2-styled.swal2-confirm,
  button.swal2-styled.swal2-cancel {
    border-radius: 26px;
    font-size: 0.875rem;
    font-weight: bold;
    width: 100%;
  }

  button.swal2-styled:focus{
    box-shadow: 0 3px 1px -2px rgba(0, 0, 0, 0.2),
      0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12) !important;
  }

  button.swal2-styled.swal2-confirm {
    background: #006400 !important;
  }

  button.swal2-styled.swal2-cancel {
    background: #fff !important;
    border: 1px solid #006400 !important;
    color: #006400 !important;
  }

`;
