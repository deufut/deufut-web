import api, { CancelToken } from '../api';

export function getRents() {
  const source = CancelToken.source();

  function apiCall({ footballfield_id, start, length }) {
    return api.post(`/rents/get/footballfield`, {
      footballfield_id,
      start,
      length,
    });
  }

  return { source, apiCall };
}

export function registerRent() {
  const source = CancelToken.source();

  function apiCall({ footballfield_id, date_start, date_end, value }) {
    return api.post(
      `/rents/register`,
      {
        footballfield_id,
        date_start,
        date_end,
        value,
      },
      { cancelToken: source.token }
    );
  }

  return { source, apiCall };
}

export function editRent() {
  const source = CancelToken.source();

  function apiCall({ rent_id, date_start, date_end, value }) {
    return api.put(
      `/rents/update`,
      {
        rent_id,
        date_start,
        date_end,
        value,
      },
      { cancelToken: source.token }
    );
  }

  return { source, apiCall };
}

export function deleteRent() {
  const source = CancelToken.source();

  function apiCall({ rentId }) {
    return api.delete(`/rents/delete/${rentId}`);
  }

  return { source, apiCall };
}

export function getRentById() {
  const source = CancelToken.source();

  function apiCall({ rentId }) {
    return api.get(`/rents/get/${rentId}`);
  }

  return { source, apiCall };
}

export function getRentRequest() {
  const source = CancelToken.source();

  function apiCall() {
    return api.get(`/rents/requests/footballfields`);
  }

  return { source, apiCall };
}

export function getRented() {
  const source = CancelToken.source();

  function apiCall() {
    return api.get(`/rents/footballfields`);
  }

  return { source, apiCall };
}

export function answerRent() {
  const source = CancelToken.source();

  function apiCall({ renter_id, answer }) {
    return api.post(`/rents/answer`, {
      renter_id,
      answer,
      team_request: false,
    });
  }

  return { source, apiCall };
}

export function kickTeam() {
  const source = CancelToken.source();

  function apiCall({ renter_id }) {
    return api.delete(`/rents/kick/${renter_id}`);
  }

  return { source, apiCall };
}
