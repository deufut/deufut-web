import api, { CancelToken } from '../api';

export function getFields() {
  const source = CancelToken.source();

  function apiCall({ start, length, search }) {
    return api.post(`/footballfields/get`, {
      start,
      length,
      filters: {
        name: search,
      },
    });
  }

  return { source, apiCall };
}

export function getFieldById() {
  const source = CancelToken.source();

  function apiCall({ fieldId }) {
    return api.get(`/footballfields/get/${fieldId}`);
  }

  return { source, apiCall };
}

export function registerField() {
  const source = CancelToken.source();

  function apiCall({
    name,
    phone,
    cnpj,
    address,
    cep,
    state,
    city,
    number,
    neighborhood,
    complement,
    description,
  }) {
    return api.post(
      `/footballfields/register`,
      {
        name,
        cnpj,
        description,
        phone,
        cep,
        state,
        city,
        address,
        number,
        neighborhood,
        complement,
        latitude: '-16.708715',
        longitude: '-49.292439',
      },
      { cancelToken: source.token }
    );
  }

  return { source, apiCall };
}

export function editField() {
  const source = CancelToken.source();

  function apiCall({
    id,
    name,
    phone,
    cnpj,
    address,
    cep,
    state,
    city,
    number,
    neighborhood,
    complement,
    description,
  }) {
    return api.put(
      `/footballfields/update`,
      {
        footballfield_id: id,
        name,
        phone,
        cnpj,
        address,
        cep,
        state,
        city,
        number,
        neighborhood,
        complement,
        description,
      },
      { cancelToken: source.token }
    );
  }

  return { source, apiCall };
}

export function deleteField() {
  const source = CancelToken.source();

  function apiCall({ fieldId }) {
    return api.delete(`/footballfields/delete/${fieldId}`);
  }

  return { source, apiCall };
}
