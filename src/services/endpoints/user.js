import api, { CancelToken } from '../api';

export function signUp() {
  const source = CancelToken.source();

  function apiCall({ name, email, password, phone }) {
    return api.post(
      `/users/register`,
      {
        name,
        email,
        password,
        phone,
      },
      { cancelToken: source.token }
    );
  }

  return { source, apiCall };
}

export function editProfile() {
  const source = CancelToken.source();

  function apiCall({ name, phone, password, email }) {
    return api.put(
      `/users/update`,
      {
        name,
        phone,
        password,
        email,
      },
      { cancelToken: source.token }
    );
  }

  return { source, apiCall };
}
